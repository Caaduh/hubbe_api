import { InitApplication } from '@main/app'
import '@test/integrations/setup'
import request from 'supertest'

describe('SignInUsecase', () => {
  const init = new InitApplication()

  afterAll(() => {
    init.socket.close()
  })
  it('should be respond with ok status and tokens to signin', async function () {
    await request(init.app)
      .post('/api/v1/user')
      .send({
        fullName: 'Cadu Alves',
        email: 'cadu.alves@gmail.com',
        password: '@Eu123456',
      })
      .set('Accept', 'application/json')

    const res = await request(init.app)
      .post('/api/v1/auth/signin')
      .send({
        email: 'cadu.alves@gmail.com',
        password: '@Eu123456',
      })
      .set('Accept', 'application/json')

    expect(res.statusCode).toBe(200)
    expect(res.body.data.accessToken).toBeTruthy()
    expect(res.body.data.refreshToken).toBeTruthy()
  })
})
