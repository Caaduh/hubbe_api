import { InitApplication } from '@main/app'
import '@test/integrations/setup'
import request from 'supertest'

const init = new InitApplication()

afterAll(() => {
  init.socket.close()
})

describe('CreateUserAccountUsecase', () => {
  it('should be respond with no content status with success', async function () {
    const res = await request(init.app)
      .post('/api/v1/user')
      .send({
        fullName: 'Cadu Alves',
        email: 'cadu.alves@gmail.com',
        password: '@Eu123456',
      })
      .set('Accept', 'application/json')

    expect(res.statusCode).toBe(204)
  })
})
