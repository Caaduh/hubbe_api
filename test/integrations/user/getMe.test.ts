import { InitApplication } from '@main/app'
import '@test/integrations/setup'
import request from 'supertest'

const init = new InitApplication()

afterAll(() => {
  init.socket.close()
})

describe('GetLoggedUserUsecase', () => {
  it('should be respond with ok status and correct data', async function () {
    await request(init.app)
      .post('/api/v1/user')
      .send({
        fullName: 'Cadu Alves',
        email: 'cadu.alves@gmail.com',
        password: '@Eu123456',
      })
      .set('Accept', 'application/json')

    const signInResponse = await request(init.app)
      .post('/api/v1/auth/signin')
      .send({
        email: 'cadu.alves@gmail.com',
        password: '@Eu123456',
      })
      .set('Accept', 'application/json')

    const getMeResponse = await request(init.app)
      .get('/api/v1/user/me')
      .send({
        authorization: signInResponse.body.data.accessToken,
      })
      .set('Accept', 'application/json')
      .set('Authorization', signInResponse.body.data.accessToken)

    expect(getMeResponse.statusCode).toBe(200)
  })
})
