import { knexConfig } from '@config/dbconfig'
import { env } from '@main/env'
import knex from 'knex'

const db = knex(knexConfig)

beforeAll(async () => {
  const exists = await db
    .select('datname')
    .from('pg_database')
    .where('datname', '=', env.db.dbDatabase)
    .first()

  if (!exists) {
    await db.raw(`CREATE DATABASE ${env.db.dbDatabase}`)
  }
})

beforeEach(async () => {
  await db.migrate.latest()
  await db.seed.run()
})

afterEach(async () => {
  await db.migrate.rollback()
})

afterAll(async () => {
  await db.destroy()
})

export { db }
