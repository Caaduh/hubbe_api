export const authTokenInputMock = {
  authorization:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsInR5cGUiOiJBRE1JTl9NQVNURVIiLCJpYXQiOjE3MDU2MTMwMTgsImV4cCI6MTcwNTYxNjYxOH0.VlQMpQWxR-gNHKDBGAI43oO_vDPtsb2u3Derbwc8KMI',
  userId: '1',
}
