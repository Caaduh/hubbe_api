import { Email } from '@domain/entity'
import '@test/unit/setup'

describe('Email', () => {
  describe('isInvalid', () => {
    it('should return false for a valid email', () => {
      const validEmail = 'john@example.com'
      const result = Email.isInvalid(validEmail)
      expect(result).toBe(false)
    })

    it('should return true for an invalid email', () => {
      const invalidEmail = 'invalidEmail'
      const result = Email.isInvalid(invalidEmail)
      expect(result).toBe(true)
    })

    it('should return true for an empty email', () => {
      const emptyEmail = ''
      const result = Email.isInvalid(emptyEmail)
      expect(result).toBe(true)
    })

    it('should return true for a null email', () => {
      const nullEmail = null
      const result = Email.isInvalid(nullEmail)
      expect(result).toBe(true)
    })

    it('should return true for an undefined email', () => {
      const undefinedEmail = undefined
      const result = Email.isInvalid(undefinedEmail)
      expect(result).toBe(true)
    })
  })
})
