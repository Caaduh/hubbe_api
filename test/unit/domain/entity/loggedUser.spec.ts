import { LoggedUser } from '@domain/entity'
import '@test/unit/setup'

describe('LoggedUser', () => {
  it('should create a LoggedUser instance with access to safe page', () => {
    const userId = 123
    const loggedUser = LoggedUser.accessSafePage(userId)
    expect(loggedUser.user).toBe(userId)
    expect(loggedUser.isLogged).toBe(true)
    expect(loggedUser.whichPage).toBe('SAFE_PAGE')
  })

  it('should create a LoggedUser instance with exit from safe page', () => {
    const userId = 123
    const loggedUser = LoggedUser.exitSafePage(userId)
    expect(loggedUser.user).toBe(userId)
    expect(loggedUser.isLogged).toBe(false)
    expect(loggedUser.whichPage).toBe('SAFE_PAGE')
  })

  it('should create a LoggedUser instance indicating being in safe page', () => {
    const userId = 123
    const loggedUser = LoggedUser.IamInSafePage(userId)
    expect(loggedUser.user).toBe(userId)
    expect(loggedUser.isLogged).toBe(true)
    expect(loggedUser.whichPage).toBe('SAFE_PAGE')
  })
})
