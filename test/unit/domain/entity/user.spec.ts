import { User } from '@domain/entity'
import {
  InvalidEmailError,
  InvalidNameError,
  InvalidPasswordError,
} from '@domain/errors'
import '@test/unit/setup'

describe('User', () => {
  describe('create', () => {
    it('should create a User instance with valid input', () => {
      const input = {
        fullName: 'Eduardo Alves',
        email: 'eduardo@hubbe.app',
        password: '@Me123456',
      }

      const result = User.create(input)

      expect(result.isRight()).toBe(true)
      expect(result.value).toBeInstanceOf(User)
    })

    it('should return InvalidEmailError when email is invalid', () => {
      const input = {
        fullName: 'Eduardo Alves',
        email: 'invalidEmail',
        password: '@Me123456',
      }

      const result = User.create(input)

      expect(result.isLeft()).toBe(true)
      expect(result.value).toBeInstanceOf(InvalidEmailError)
    })

    it('should return InvalidPasswordError when password is invalid', () => {
      const input = {
        fullName: 'Eduardo Alves',
        email: 'eduardo@hubbe.app',
        password: 'invalidPassword',
      }

      const result = User.create(input)

      expect(result.isLeft()).toBe(true)
      expect(result.value).toBeInstanceOf(InvalidPasswordError)
    })

    it('should return InvalidNameError when full name is invalid', () => {
      const input = {
        fullName: 'Eduardo',
        email: 'eduardo@hubbe.app',
        password: '@Me123456',
      }

      const result = User.create(input)

      expect(result.isLeft()).toBe(true)
      expect(result.value).toBeInstanceOf(InvalidNameError)
    })
  })

  describe('signIn', () => {
    it('should sign in with valid email and password', () => {
      const email = 'eduardo@hubbe.app'
      const password = '@Me123456'

      const result = User.signIn(email, password)

      expect(result.isRight()).toBe(true)
    })

    it('should return InvalidEmailError when email is invalid', () => {
      const email = 'invalidEmail'
      const password = '@Me123456'

      const result = User.signIn(email, password)

      expect(result.isLeft()).toBe(true)
      expect(result.value).toBeInstanceOf(InvalidEmailError)
    })
  })

  describe('passwordIsInvalid', () => {
    it('should return right with false if password is valid', () => {
      const password = '@Me123456'

      const result = User.passwordIsInvalid(password)

      expect(result.isRight()).toBe(true)
      expect(result.value).toBe(false)
    })

    it('should return left with InvalidPasswordError if password is empty', () => {
      const password = ''

      const result = User.passwordIsInvalid(password)

      expect(result.isLeft()).toBe(true)
      expect(result.value).toBeInstanceOf(InvalidPasswordError)
    })

    it('should return left with InvalidPasswordError if password does not meet criteria', () => {
      const password = 'invalidPassword'

      const result = User.passwordIsInvalid(password)

      expect(result.isLeft()).toBe(true)
      expect(result.value).toBeInstanceOf(InvalidPasswordError)
    })
  })
})
