import { SafePageBusyError } from '@application/errors'
import { InvalidTokenError } from '@application/errors/invalidToken.error'
import { UserLoggedOnSafePageUsecase } from '@application/usecases/user'
import { left, right } from '@domain/either'
import { UserLoggedOnSafePagePage } from '@domain/usecases/user/loggedOnSafePage.protocol'
import { ProcessEntityError } from '@infrastructure/errors/processEntity.error'
import '@test/unit/setup'

const makeSut = () => {
  const input = {
    authorization: 'validToken',
  }

  const jwtAdapter: any = {
    verify: jest.fn().mockReturnValue(true),
    decrypt: jest.fn().mockReturnValue({ userId: 1 }),
  }

  const loggedUserRepository: any = {
    inSafePage: jest.fn().mockResolvedValueOnce(left(null)),
    insert: jest.fn().mockResolvedValueOnce(right({ id: 1 })),
  }

  const sut: UserLoggedOnSafePagePage = new UserLoggedOnSafePageUsecase(
    loggedUserRepository,
    jwtAdapter
  )

  return { sut, loggedUserRepository, jwtAdapter, input }
}

describe('UserLoggedOnSafePageUsecase', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should return right with success message if token is valid and user is successfully connected to safe page', async () => {
    const { input, sut, loggedUserRepository } = makeSut()
    jest
      .spyOn(loggedUserRepository, 'inSafePage')
      .mockResolvedValueOnce(left(new SafePageBusyError()))

    const result = await sut.handle(input)

    expect(result.isRight()).toBeTruthy()
    expect(result.value).toEqual('ConnectedSafePage')
  })

  it('should return left with SafePageBusyError if user is already connected to safe page', async () => {
    const { input, sut, loggedUserRepository } = makeSut()

    loggedUserRepository.inSafePage = jest
      .fn()
      .mockReturnValueOnce(right({ id: 1 }))

    const result = await sut.handle(input)

    expect(result.isLeft()).toBeTruthy()
    expect(result.value).toBeInstanceOf(SafePageBusyError)
  })

  it('should return left with InvalidTokenError if token is invalid', async () => {
    const { input, sut, jwtAdapter } = makeSut()
    jest.spyOn(jwtAdapter, 'verify').mockReturnValueOnce(false)

    const result = await sut.handle(input)

    expect(result.isLeft()).toBeTruthy()
    expect(result.value).toBeInstanceOf(InvalidTokenError)
  })

  it('should not create a logged user if there is an error on database', async () => {
    const { sut, loggedUserRepository, input } = makeSut()

    loggedUserRepository.insert = jest
      .fn()
      .mockReturnValueOnce(left(new ProcessEntityError()))

    const result = await sut.handle(input)

    expect(result.isLeft()).toBe(true)
    expect(result.value).toBeInstanceOf(ProcessEntityError)
  })
})
