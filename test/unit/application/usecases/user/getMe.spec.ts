import { UserNotFoundError } from '@application/errors'
import { GetLoggedUserUsecase } from '@application/usecases/user'
import { left, right } from '@domain/either'
import { GetLoggedUser } from '@domain/usecases/user'
import '@test/unit/setup'

const makeSut = () => {
  const input: any = {
    userId: '1',
  }

  const userRepository: any = {
    get: jest
      .fn()
      .mockResolvedValueOnce(
        right({ id: 1, fullName: 'Eduardo Alves', email: 'eduardo@hubbe.app' })
      ),
  }

  const sut: GetLoggedUser = new GetLoggedUserUsecase(userRepository)

  return { sut, userRepository, input }
}

describe('GetLoggedUserUsecase', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should return right with found user if user exists', async () => {
    const { sut, input } = makeSut()

    const result = await sut.handle(input)

    expect(result.isRight()).toBeTruthy()
    expect(result.value).toEqual({
      fullName: 'Eduardo Alves',
      email: 'eduardo@hubbe.app',
    })
  })

  it('should return left with UserNotFoundError if user does not exist', async () => {
    const { sut, userRepository, input } = makeSut()
    userRepository.get = jest
      .fn()
      .mockResolvedValue(left(new UserNotFoundError()))

    const result = await sut.handle(input)

    expect(result.isLeft()).toBeTruthy()
    expect(result.value).toBeInstanceOf(UserNotFoundError)
  })
})
