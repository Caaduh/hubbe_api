import { InvalidTokenError } from '@application/errors'
import { VerifySafePageStatusUsecase } from '@application/usecases/user/verifySafePageStatus.usecase'
import { left, right } from '@domain/either'

const makeSut = () => {
  const jwtAdapter: any = {
    verify: jest.fn().mockReturnValue(true),
  }

  const loggedUserRepository: any = {
    inSafePage: jest.fn(),
  }

  const sut = new VerifySafePageStatusUsecase(loggedUserRepository, jwtAdapter)

  return { sut, jwtAdapter, loggedUserRepository }
}

describe('VerifySafePageStatusUsecase', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should return right with SafePageBusy message when token is valid and someone is on safe page', async () => {
    const { loggedUserRepository, sut } = makeSut()
    loggedUserRepository.inSafePage.mockResolvedValueOnce(right({ id: 1 }))

    const result = await sut.handle({ authorization: 'validToken' })

    expect(result.isRight()).toBeTruthy()
    expect(result.value).toBe('SafePageBusy')
  })

  it('should return left with InvalidTokenError when token is invalid', async () => {
    const { jwtAdapter, sut } = makeSut()
    jwtAdapter.verify.mockReturnValueOnce(false)

    const result = await sut.handle({ authorization: 'invalidToken' })

    expect(result.isLeft()).toBeTruthy()
    expect(result.value).toBeInstanceOf(InvalidTokenError)
  })

  it('should return right with SafePageFree message when token is valid and there is no one on safe page', async () => {
    const { sut, loggedUserRepository } = makeSut()
    loggedUserRepository.inSafePage.mockResolvedValueOnce(left(null))

    const result = await sut.handle({ authorization: 'validToken' })

    expect(result.isRight()).toBeTruthy()
    expect(result.value).toBe('SafePageFree')
  })
})
