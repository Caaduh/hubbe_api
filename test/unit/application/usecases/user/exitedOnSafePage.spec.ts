import {
  InvalidTokenError,
  NotLoggedOnSafePageError,
} from '@application/errors'
import { UserExitedOnSafePageUsecase } from '@application/usecases/user'
import { left, right } from '@domain/either'
import { ProcessEntityError } from '@infrastructure/errors/processEntity.error'
import '@test/unit/setup'

const makeSut = () => {
  const successParams = {
    fullName: 'Eduardo Alves',
    email: 'eduardo@hubbe.com',
    password: '@Eu123456',
  }

  const loggedUserRepository: any = {
    verifyLoggedSafePage: jest.fn().mockReturnValueOnce(right(true)),
    exitSafePage: jest.fn().mockReturnValueOnce(right(true)),
  }

  const jwtAdapter: any = {
    verify: jest.fn().mockReturnValueOnce(true),
    decrypt: jest.fn().mockReturnValueOnce({ userId: 1 }),
  }

  const sut = new UserExitedOnSafePageUsecase(loggedUserRepository, jwtAdapter)

  return { sut, loggedUserRepository, jwtAdapter, successParams }
}

describe('UserExitedOnSafePageUsecase', () => {
  it('should return right with success message if all conditions are met', async () => {
    const { sut } = makeSut()
    const result = await sut.handle({ authorization: 'validToken' })
    expect(result.isRight()).toBeTruthy()
    expect(result.value).toEqual('SafePageReleased')
  })

  it('should return NotLoggedOnSafePageError if LoggedUserRepository.verifyLoggedSafePage returns a left value', async () => {
    const { sut, loggedUserRepository } = makeSut()

    loggedUserRepository.verifyLoggedSafePage = jest
      .fn()
      .mockReturnValueOnce(left(true))

    const result = await sut.handle({
      authorization: 'invalidToken',
    })

    expect(result.isLeft()).toBe(true)
    expect(result.value).toBeInstanceOf(NotLoggedOnSafePageError)
  })
  it('should return InvalidTokenError if token is invalid', async () => {
    const { sut, jwtAdapter } = makeSut()

    jwtAdapter.verify = jest.fn().mockReturnValueOnce(false)

    const result = await sut.handle({ authorization: 'invalidToken' })
    expect(result.value).toBeInstanceOf(InvalidTokenError)
  })

  it('should return SafePageBusy if error on update exit safe page', async () => {
    const { sut, loggedUserRepository } = makeSut()

    loggedUserRepository.exitSafePage = jest
      .fn()
      .mockReturnValueOnce(left(new ProcessEntityError()))

    const result = await sut.handle({ authorization: 'validToken' })
    expect(result.value).toBe('SafePageBusy')
  })
})
