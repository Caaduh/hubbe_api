import { UserExistsError } from '@application/errors'
import { CreateUserAccountUsecase } from '@application/usecases/user'
import { left, right } from '@domain/either'
import { InvalidEmailError } from '@domain/errors'
import { ProcessEntityError } from '@infrastructure/errors/processEntity.error'
import '@test/unit/setup'

const makeSut = () => {
  const successParams = {
    fullName: 'Eduardo Alves',
    email: 'eduardo@hubbe.com',
    password: '@Eu123456',
  }

  const userRepository: any = {
    getByEmail: jest.fn().mockReturnValueOnce(left(null)),
    save: jest.fn().mockReturnValueOnce(right(true)),
  }

  const hashAdapter = {
    generate: jest.fn().mockReturnValueOnce('mockedPasswordInPlainText'),
  }

  const sut = new CreateUserAccountUsecase(userRepository, hashAdapter)

  return { sut, userRepository, hashAdapter, successParams }
}

describe('CreateUserAccountUsecase', () => {
  it('should create a user successfully', async () => {
    const { sut, hashAdapter, userRepository, successParams } = makeSut()

    const result = await sut.handle(successParams)

    expect(userRepository.getByEmail).toHaveBeenCalledWith(successParams.email)
    expect(hashAdapter.generate).toHaveBeenCalledWith(successParams.password)
    expect(userRepository.save).toHaveBeenCalledWith(expect.anything())
    expect(result.isRight()).toBe(true)
    expect(result.value).toBe(true)
  })

  it('should not create a user if user already exists', async () => {
    const { sut, userRepository, successParams } = makeSut()

    userRepository.getByEmail = jest.fn().mockReturnValueOnce(right({ id: 1 }))

    const result = await sut.handle(successParams)

    expect(result.isLeft()).toBe(true)
    expect(result.value).toBeInstanceOf(UserExistsError)
  })

  it('should not create a user if there is an error on database', async () => {
    const { sut, userRepository, successParams } = makeSut()

    userRepository.save = jest
      .fn()
      .mockReturnValueOnce(left(new ProcessEntityError()))

    const result = await sut.handle(successParams)

    expect(result.isLeft()).toBe(true)
    expect(result.value).toBeInstanceOf(ProcessEntityError)
  })

  it('should not create a user if there is an error on validate input', async () => {
    const { sut, successParams } = makeSut()

    const result = await sut.handle({
      ...successParams,
      email: 'invalidEmail',
    })

    expect(result.isLeft()).toBe(true)
    expect(result.value).toBeInstanceOf(InvalidEmailError)
  })
})
