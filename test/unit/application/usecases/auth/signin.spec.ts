import { InvalidLoginError, UserNotFoundError } from '@application/errors'
import { SignInUsecase } from '@application/usecases/auth'
import { left, right } from '@domain/either'
import { InvalidEmailError } from '@domain/errors'
import { authTokenInputMock } from '@test/mocks/authToken.mock'

const makeSut = () => {
  const successParams = {
    fullName: 'Eduardo Alves',
    email: 'eduardo@hubbe.com',
    password: '@Eu123456',
  }

  const userRepository: any = {
    getByEmail: jest.fn().mockReturnValueOnce(right({ id: 1 })),
  }

  const hashCompare = {
    compare: jest.fn().mockResolvedValueOnce(true),
  }

  const jwt = {
    encrypt: jest.fn().mockReturnValueOnce({
      accessToken: authTokenInputMock.authorization,
      refreshToken: authTokenInputMock.authorization,
    }),
  }

  const sut = new SignInUsecase(userRepository, hashCompare, jwt)

  return { sut, userRepository, hashCompare, jwt, successParams }
}

describe('SignInUsecase', () => {
  it('should be signin with success', async () => {
    const { sut, successParams } = makeSut()

    const result = await sut.handle(successParams)
    expect(result.value).toEqual({
      accessToken: authTokenInputMock.authorization,
      refreshToken: authTokenInputMock.authorization,
    })
  })

  it('should not be a success login if input validate error', async () => {
    const { sut, successParams } = makeSut()

    const result = await sut.handle({
      ...successParams,
      email: 'invalidEmail',
    })
    expect(result.isLeft()).toBe(true)
    expect(result.value).toBeInstanceOf(InvalidEmailError)
  })

  it('should not be a success login if user not found', async () => {
    const { sut, successParams, userRepository } = makeSut()

    userRepository.getByEmail = jest.fn().mockReturnValueOnce(left(null))

    const result = await sut.handle(successParams)
    expect(result.isLeft()).toBe(true)
    expect(result.value).toBeInstanceOf(UserNotFoundError)
  })

  it('should not be a success login if password does not match', async () => {
    const { sut, successParams, hashCompare } = makeSut()

    hashCompare.compare = jest.fn().mockReturnValueOnce(false)

    const result = await sut.handle(successParams)
    expect(result.isLeft()).toBe(true)
    expect(result.value).toBeInstanceOf(InvalidLoginError)
  })
})
