<h1 align="center">🚀 May the force be with you 🚀</h1>

## Author

### Made with ❤️ by Eduardo Alves 👋 [@codespoa](https://github.com/codespoa)

This API was developed using some design patterns, along with a basic hexagonal architecture, with error handling using a functional programming technique called Either. In this API, we basically have three REST routes: one for creating a user, one for login, and another for retrieving the user who is currently logged in to the application. It is also running a WebSocket using the WS library, as it is a simpler test, I did not opt for websocket or something more robust.

`For this project we will use fio, but if you prefer npm just change the command`

### Techs
- [x] [Node.js]
- [x] [Typescript]
- [x] [Postgres]
- [x] [Docker]
- [x] [Express]
- [x] [WS]
- [x] [Jest]
- [x] [Supertest]
- [x] [JWT]
- [x] [Bcrypt]
- [x] [Winston Logger]
- [x] [ESlint]
- [x] [Swagger]

## Requirements

- Node.js version: v16.14.2
- Docker
- docker-compose

## Getting Started

Clone this repository

```bash
git clone 
```
Access the project folder

```bash
cd hubbe_api 
```

Create the `.env` file from `.env.example` and fill in the requested environment variables

```bash
cp .env.example .env
```

Fill in the missing information in the env file

## Dependencies

Install all dependencies for the project

```sh
yarn
```

## Starting the Application

run migrations

```bash
yarn migrate:latest
```


### To run

After all environment variables are configured, run

```sh
yarn server:up
```

your application its running on:

```sh
http://localhost/${PORT}
```

and your websocket on:
```sh
ws://localhost/${WEBSOCKET_PORT}
```

``Note: if you are having Cors problems on your local network with any type of browser (chrome), I suggest using Firefox with an extension to release Cors, because even with all Cors policies released for development, the browser would block.``

### The websocket event are

`userLoggedOnSafePage - To logged in safe page`
`userExitedOnSafePage - To exit safe page`

## Tests

The project was designed with unit tests and integration tests, using jest and supertest. To run the tests is quite simple.

### All Tests

```sh
yarn test
```

### Unit Tests

```sh
yarn test:unit
```

### Integration Tests

```sh
yarn test:integration
```

### Coverage

```sh
yarn test:coverage
```

### Sonar

This application relies on Sonarqube, a static source code analysis platform designed to help developers write cleaner, higher quality code

to get up the container, run

```bash
  docker-compose up -d sonarqube
```

and then

```bash
  yarn sonar
```

#### OBSERVATIONS

`` Remember: you need to do the initial sonar configurations, check this link: ``

[SONAR DOCS](https://cloud.ibm.com/docs/ContinuousDelivery?topic=ContinuousDelivery-sonarqube&locale=pt-BR)

## Documentation

You can use Swagger running at

```sh
http://localhost:{PORT}/api/v1/docs/#
```

But if you want, you can use the Documentation here in the Readme

```http
  POST /api/v1/user
```

| Parameter   | Type       | Description    |
| :---------- | :--------- | :------------- |
| `fullName`  | `string`   | **Required**   |
| `email`     | `string`   | **Required**   |
| `password`  | `string`   | **Required**   |

#### Returns a 204 status

`|------------------------------------------|`

```http
  POST /api/v1/user/me
```

| Parameter        | Type       | Description    |
| :----------------| :--------- | :------------- |
| `authorization`  | `string`   | **Required**   |

#### Returns your profile data

```txt
{
  "data": {
    "fullName": "Eduardo Alves",
    "email": "eduardo@hubbe.app"
  }
}
```

`|------------------------------------------|`

```http
  POST /api/v1/signin
```

| Parameter   | Type       | Description    |
| :---------- | :--------- | :------------- |
| `email`     | `string`   | **Required**   |
| `password`  | `string`   | **Required**   |

#### Returns a 200 status with the following fields

```txt
{
  "data": {
    "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTcwOTUwODA3MSwiZXhwIjoxNzA5NTExNjcxfQ.FeXiYtWyHLOi1He-2-9LZ7aNlHlqqW_4L43005fqVi0",
    "refreshToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTcwOTUwODA3MSwiZXhwIjoxNzEwMTEyODcxfQ.Wjz3RLwYX_PuBo9fI2YD4jdFXN2e_JKRwIbdm6bUVdY"
  }
}
```

