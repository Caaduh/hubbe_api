require('dotenv').config()
const { compilerOptions } = require('./tsconfig.json')
const { pathsToModuleNameMapper } = require('ts-jest')

module.exports = {
  bail: 1,
  clearMocks: true,
  coverageProvider: 'babel',
  collectCoverageFrom: [
    'src/application/usecases/**/*',
    'src/domain/entity/**/*',
  ],
  coverageDirectory: 'coverage',
  coveragePathIgnorePatterns: [
    'coverage',
    'node_modules',
    'build',
    '/index\\.ts$',
  ],
  coverageReporters: ['json', 'text', 'lcov', 'clover'],
  coverageThreshold: {
    global: {
      branches: 95,
      functions: 95,
      lines: 95,
      statements: 95,
    },
  },
  moduleDirectories: ['node_modules'],
  moduleFileExtensions: ['js', 'ts', 'node'],
  testPathIgnorePatterns: ['/node_modules/'],
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths, {
    prefix: '<rootDir>',
  }),
  transform: {
    '.+\\.ts$': 'ts-jest',
  },
  testEnvironment: 'node',
}
