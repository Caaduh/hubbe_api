module.exports = {
  semi: false,
  singleQuote: true,
  trailingComma: 'es5',
  arrowParens: 'avoid',
  bracketSameLine: false,
  endOfLine: 'auto',
}
