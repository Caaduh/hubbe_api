import { StatusTypeEnum } from '@presenters/enums/errors.enum'
import { HttpEnum } from '@presenters/enums/http.enum'

export const badRequest = (fields: any): any => {
  let allFields: { fieldName: string; message: string }[] = fields ?? []

  if (Array.isArray(fields) && fields?.length > 1) {
    allFields = fields?.map(field => {
      return {
        fieldName: field?.field,
        message: field?.message,
      }
    })
  }

  return {
    statusCode: HttpEnum.BAD_REQUEST,
    data: { type: 'BadRequestError', error: allFields },
  }
}

export const unauthorized = (message?: string): any => ({
  statusCode: HttpEnum.UNAUTHORIZED,
  data: {
    error: {
      type: 'UnauthorizedError',
      message: message || 'Acesso negado, autenticação falhou',
    },
  },
})

export const notFound = (message: string): any => ({
  statusCode: HttpEnum.NOT_FOUND,
  data: { error: { type: 'NotFoundError', message } },
})

export const serverError = (): any => ({
  statusType: StatusTypeEnum.SERVER_ERROR,
  data: { error: 'Internal Server Error' },
})

export const ok = (data?: any): any => ({
  statusCode: HttpEnum.OK,
  data: data ?? null,
})

export const error = (statusType: string, error: any): any => ({
  statusType: statusType,
  data: error,
})

export const created = (): any => ({
  statusCode: HttpEnum.CREATED,
  data: null,
})

export const accepted = (data: any): any => ({
  statusCode: HttpEnum.ACCEPTED,
  data: data,
})

export const conflict = (message = 'Informação já existente'): any => ({
  statusCode: HttpEnum.CONFLICT,
  data: { error: { type: 'ConflictError', message: message } },
})

export const unprocessableContent = (error: Error): any => ({
  statusCode: HttpEnum.UNPROCESSABLE_CONTENT,
  data: {
    error: {
      type: 'UnprocessableContentError',
      message:
        error?.message ||
        'Hmmm, Um erro inesperado na integração com algum de nossos parceiros',
    },
  },
})

export const noContent = (): any => ({
  statusCode: HttpEnum.NO_CONTENT,
})

export const forbidden = (message: string): any => ({
  statusCode: HttpEnum.FORBIDDEN,
  data: { error: { type: 'ForbiddenError', message } },
})
