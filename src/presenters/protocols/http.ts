export interface HttpResponse<D> {
  statusCode?: number
  data?: D
}
