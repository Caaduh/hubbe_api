export interface WebsocketProtocol {
  connect: () => void
  close: () => void
}
