export interface MiddlewareProtocol {
  handle: (httpRequest: any) => Promise<any>
}
