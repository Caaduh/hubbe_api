import {
  badRequest,
  conflict,
  created,
  forbidden,
  notFound,
  serverError,
  unauthorized,
  unprocessable,
} from '@presenters/docs/components/index'
import { apiKeyAuthSchema } from '@presenters/docs/schemas/index'

export default {
  securitySchemes: {
    apiKeyAuth: apiKeyAuthSchema,
  },
  badRequest,
  serverError,
  unauthorized,
  notFound,
  forbidden,
  conflict,
  unprocessable,
  created,
}
