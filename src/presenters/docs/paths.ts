import * as paths from '@presenters/docs/paths/index'

export default {
  // AUTH
  '/auth/signin': paths.signInPath,

  // USER
  '/user': paths.userWithoutIdPath,
  '/user/me': paths.getLoggedUserPath,
}
