import components from '@presenters/docs/components'
import paths from '@presenters/docs/paths'
import schemas from '@presenters/docs/schemas'

export const swaggerConfig = {
  openapi: '3.0.0',
  info: {
    title: 'Hubbe',

    version: '1.0.0',
    license: {
      name: 'SLA - Service Level Agreement',
    },
  },
  servers: [
    {
      url: '/api/v1',
      description: 'Local Server',
    },
  ],
  tags: [
    {
      name: 'Auth',
    },

    {
      name: 'User',
    },
  ],
  paths,
  schemas,
  components,
}
