import * as docSchema from '@presenters/docs/schemas/index'

export default {
  badRequest: docSchema.badRequestSchema,
  notFound: docSchema.notFoundSchema,
  unprocessable: docSchema.unprocessableSchema,
  serverError: docSchema.errorSchema,
  signInParamsSchema: docSchema.signInParamsSchema,
  signInResultSchema: docSchema.signInResultSchema,
  createUserParamsSchema: docSchema.createUserParamsSchema,
  getUserLoggedUserResultSchema: docSchema.getUserLoggedUserResultSchema,
}
