export const userWithoutIdPath = {
  post: {
    tags: ['User'],
    security: [
      {
        apiKeyAuth: [],
      },
    ],
    description: 'Rota usada para criar um usuário',
    requestBody: {
      required: true,
      content: {
        'application/json': {
          schema: {
            $ref: '#/schemas/createUserParamsSchema',
          },
        },
      },
    },
    responses: {
      204: {
        description: 'OK',
      },
      400: {
        $ref: '#/components/badRequest',
      },
      401: {
        $ref: '#/components/unauthorized',
      },
      403: {
        $ref: '#/components/forbidden',
      },
      404: {
        $ref: '#/components/notFound',
      },
      422: {
        $ref: '#/components/unprocessable',
      },
      500: {
        $ref: '#/components/serverError',
      },
    },
  },
}
