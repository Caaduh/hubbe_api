export const getLoggedUserPath = {
  get: {
    tags: ['User'],
    security: [
      {
        apiKeyAuth: [],
      },
    ],
    description: 'Rota usada para buscar o usuário logado',
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              $ref: '#/schemas/getUserLoggedUserResultSchema',
            },
          },
        },
      },
      400: {
        $ref: '#/components/badRequest',
      },
      401: {
        $ref: '#/components/unauthorized',
      },
      403: {
        $ref: '#/components/forbidden',
      },
      404: {
        $ref: '#/components/notFound',
      },
      422: {
        $ref: '#/components/unprocessable',
      },
      500: {
        $ref: '#/components/serverError',
      },
    },
  },
}
