export const signInPath = {
  post: {
    tags: ['Auth'],
    description: 'Rota usada logar um usuário',
    requestBody: {
      required: true,
      content: {
        'application/json': {
          schema: {
            $ref: '#/schemas/signInParamsSchema',
          },
        },
      },
    },
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              $ref: '#/schemas/signInResultSchema',
            },
          },
        },
      },
      400: {
        $ref: '#/components/badRequest',
      },
      401: {
        $ref: '#/components/unauthorized',
      },
      403: {
        $ref: '#/components/forbidden',
      },
      404: {
        $ref: '#/components/notFound',
      },
      422: {
        $ref: '#/components/unprocessable',
      },
      500: {
        $ref: '#/components/serverError',
      },
    },
  },
}
