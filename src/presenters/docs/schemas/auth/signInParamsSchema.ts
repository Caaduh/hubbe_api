export const signInParamsSchema = {
  type: 'object',
  properties: {
    email: {
      type: 'string',
      example: 'eduardo@hubbe.app',
    },
    password: {
      type: 'string',
      example: '@MasterAdm_202',
    },
  },
  required: ['email', 'password'],
}
