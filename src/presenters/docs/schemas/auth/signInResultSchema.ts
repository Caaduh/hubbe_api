export const signInResultSchema = {
  type: 'object',
  properties: {
    data: {
      type: 'object',
      properties: {
        accessToken: {
          type: 'string',
        },
        refreshToken: {
          type: 'string',
        },
      },
    },
  },
}
