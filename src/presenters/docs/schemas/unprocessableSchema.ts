export const unprocessableSchema = {
  type: 'object',
  properties: {
    data: {
      type: 'object',
      properties: {
        error: {
          type: 'object',
          properties: {
            type: {
              type: 'string',
              example: 'UnprocessableError',
            },
            message: {
              type: 'string',
            },
          },
        },
      },
    },
  },
}
