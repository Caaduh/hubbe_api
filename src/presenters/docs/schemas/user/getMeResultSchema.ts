export const getUserLoggedUserResultSchema = {
  type: 'object',
  properties: {
    fullName: {
      type: 'string',
      example: 'Eduardo Alves',
    },
    email: {
      type: 'string',
      example: 'eduardo@hubbe.app',
    },
  },
}
