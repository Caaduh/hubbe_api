export const createUserParamsSchema = {
  type: 'object',
  properties: {
    fullName: {
      type: 'string',
      example: 'Eduardo Alves',
    },
    email: {
      type: 'string',
      example: 'eduardo@hubbe.app',
    },
    password: {
      type: 'string',
      example: '@MasterAdm_202',
    },
  },
  required: ['email', 'password', 'fullName'],
}
