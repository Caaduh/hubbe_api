export const errorSchema = {
  type: 'object',
  properties: {
    data: {
      type: 'object',
      properties: {
        error: {
          type: 'object',
          properties: {
            type: {
              type: 'string',
            },
            message: {
              type: 'string',
            },
          },
        },
      },
    },
  },
  required: ['error'],
}
