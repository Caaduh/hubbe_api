export const createdSchema = {
  type: 'object',
  properties: {
    data: {
      type: 'string',
      nullable: true,
      example: 'null',
    },
  },
}
