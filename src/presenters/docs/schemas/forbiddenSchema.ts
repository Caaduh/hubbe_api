export const forbiddenSchema = {
  type: 'object',
  properties: {
    data: {
      type: 'object',
      properties: {
        error: {
          type: 'object',
          properties: {
            type: {
              type: 'string',
              example: 'ForbiddenErrorError',
            },
            message: {
              type: 'string',
            },
          },
        },
      },
    },
  },
}
