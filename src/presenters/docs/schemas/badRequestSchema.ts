export const badRequestSchema = {
  type: 'object',
  properties: {
    data: {
      type: 'object',
      properties: {
        error: {
          type: 'object',
          properties: {
            type: {
              type: 'string',
              example: 'BadRequestError',
            },
            message: {
              type: 'string',
            },
          },
        },
      },
    },
  },
}
