export const forbidden = {
  description: 'Não autorizado',
  content: {
    'application/json': {
      schema: {
        $ref: '#/schemas/badRequest',
      },
    },
  },
}
