export const conflict = {
  description: 'Essa informação já existe',
  content: {
    'application/json': {
      schema: {
        $ref: '#/schemas/serverError',
      },
    },
  },
}
