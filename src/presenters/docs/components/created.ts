export const created = {
  description: 'Criado com sucesso',
  content: {
    'application/json': {
      schema: {
        $ref: '#/schemas/created',
      },
    },
  },
}
