export const unprocessable = {
  description: 'Erro ao tenar processar a request',
  content: {
    'application/json': {
      schema: {
        $ref: '#/schemas/unprocessable',
      },
    },
  },
}
