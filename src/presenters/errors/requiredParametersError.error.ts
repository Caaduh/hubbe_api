import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class RequiredParametersError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor(message: string) {
    super(message)
    this.message = message
    this.errorType = StatusTypeEnum.REQUIRED_PARAMETERS_ERROR
  }
}
