import express from 'express'

import { makeAuthRoute } from '@main/factories/main/routes/auth.factory'
import { makeDocsRoute } from '@main/factories/main/routes/docs.factory'
import { makeUserRoute } from '@main/factories/main/routes/user.factory'

export class HttpServer {
  private readonly controllers: any[]

  constructor() {
    this.controllers = [makeDocsRoute(), makeAuthRoute(), makeUserRoute()]
  }

  httpApp(): express.Application {
    const app = express()

    app.use(express.json({ limit: '10mb' }))

    this.controllers.forEach((route: any) => {
      const router = express.Router()
      route.register(router)
      app.use(router)
    })

    return app
  }
}
