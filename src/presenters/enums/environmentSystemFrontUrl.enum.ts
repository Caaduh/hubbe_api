export enum EnvironmentSystemFrontUrlEnum {
  local = 'http://localhost:3001',
  develop = 'https://dashboard.dev.quandoeupartir.com',
  homolog = 'https://dashboard.homolog.quandoeupartir.com',
  prod = 'https://dashboard.quandoeupartir.com',
}
