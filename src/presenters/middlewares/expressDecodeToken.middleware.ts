import { expressMiddlewareAdapter } from '@main/adapters/expressMiddleware.adapter'
import { makeExpressDecodeTokenMiddleware } from '@main/factories/main/middlewares/decodedToken.middleware'

export const expressDecodeTokenMiddleware = expressMiddlewareAdapter(
  makeExpressDecodeTokenMiddleware()
)
