import { WebsocketEventsEnum } from '@domain/enums/websocketEvents.enum'
import { Logger } from '@infrastructure/adapters'
import { Socket } from '@infrastructure/adapters/socket.protocol'
import { WebsocketEventsStrategy } from '@infrastructure/strategies/websocketEvents.strategy'
import { WebsocketProtocol } from '@presenters/protocols/websocket'
import { WebsocketEntryValidator } from '@presenters/validators/websocket.validator'

export class WebSocket implements WebsocketProtocol {
  private readonly HEARTBEAT_INTERVAL = 1000 * 5
  private readonly HEARTBEAT_VALUE = 'ping'
  private readonly CLIENT_CONNECTED_ON_SOCKET = 1

  constructor(
    private readonly webSocketAdapter: Socket,
    private readonly logger: Logger
  ) {}

  connect() {
    const connection = this.webSocketAdapter.getConnection()

    connection.on('connection', socket => {
      socket.isAlive = true

      this.setupMessageHandler(socket, connection)

      const interval = setInterval(() => {
        connection.clients.forEach(client => {
          if (!client.isAlive) {
            client.isAlive = false
            connection.clients.delete(client)
            return
          }

          client.isAlive = true
          this.ping(client)
        })
      }, this.HEARTBEAT_INTERVAL)

      socket.on('close', () => {
        clearInterval(interval)
      })
    })
  }

  close(): void {
    this.webSocketAdapter.closeConnection()
  }

  private ping(socket) {
    socket.send(this.HEARTBEAT_VALUE)
  }

  private setupMessageHandler(socket, connection) {
    socket.on('message', async message => {
      const convertedMessageToString = message?.toString()

      try {
        if (convertedMessageToString === 'pong') {
          socket.isAlive = true
        } else {
          const parsedMessage = JSON.parse(convertedMessageToString)

          const validator = WebsocketEntryValidator.validate(parsedMessage)

          if (validator.isLeft()) {
            socket.send(validator.value.message)
            return
          }

          const result = await WebsocketEventsStrategy.handle(
            validator.value.event,
            validator.value.data
          )

          if (result?.isRight()) {
            const message: string =
              typeof result.value === 'string' && result.value
            if (message === WebsocketEventsEnum.VERIFY_SAFE_PAGE_STATUS) {
              connection.clients.forEach(client => {
                if (client.readyState === 1) {
                  client.send('SafePageBusy')
                }
              })
            }
            socket.send(result.value)

            if (message === 'ConnectedSafePage') {
              connection.clients.forEach(client => {
                if (client.readyState === this.CLIENT_CONNECTED_ON_SOCKET) {
                  client.send('SafePageBusy')
                }
              })
            }

            if (message === 'SafePageReleased') {
              connection.clients.forEach(client => {
                if (client.readyState === this.CLIENT_CONNECTED_ON_SOCKET) {
                  client.send('SafePageFree')
                }
              })
            }

            if (result?.isLeft()) {
              socket.send(result.value.message)
            }
          }
        }

        connection.clients.forEach(client => {
          if (client.readyState === this.CLIENT_CONNECTED_ON_SOCKET) {
            client.send(convertedMessageToString)
          }
        })
      } catch (error) {
        this.logger.error(`[X] WebSocket.message: ${error}`)
        socket.send('Error on process message, try again later')
        socket.close()
      }
    })
  }
}
