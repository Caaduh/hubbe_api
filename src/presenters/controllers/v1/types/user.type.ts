import { UserNotFoundError } from '@application/errors'
import { Right } from '@domain/either'
import { InvalidEmailError } from '@domain/errors'
import { BaseInputData } from '@domain/types/baseInputData.protocol'
import { CreateUserAccountUsecaseInput } from '@domain/usecases/user'
import { StatusTypeEnum } from '@presenters/enums/errors.enum'

type PossibleErrors = UserNotFoundError | InvalidEmailError

export type CreateUserAccountControllerRequest = CreateUserAccountUsecaseInput

export type EitherCreateUserAccountResponse = {
  statusType: StatusTypeEnum
}

type GetLoggedUserControllerData = {
  accessToken: string
  refreshToken: string
}

export type GetLoggedUserControllerSuccessResponse = {
  data: GetLoggedUserControllerData
}

type GetLoggedUserControllerFailureResponse = PossibleErrors

export type GetLoggedUserControllerRequest = BaseInputData

export type EitherGetLoggedUserControllerResponse = {
  statusType: StatusTypeEnum
  data:
    | GetLoggedUserControllerData
    | Right<
        GetLoggedUserControllerFailureResponse,
        GetLoggedUserControllerSuccessResponse
      >
}
