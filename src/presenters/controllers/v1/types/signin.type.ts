import { InvalidLoginError, UserNotFoundError } from '@application/errors'
import { Right } from '@domain/either'
import { StatusTypeEnum } from '@presenters/enums/errors.enum'

type Data = {
  accessToken: string
  refreshToken: string
}

export type SignInControllerSuccessResponse = {
  data: Data
}

type FailureResponse = UserNotFoundError | InvalidLoginError

export type SignInControllerRequest = {
  email: string
  password: string
}

export type EitherSignInControllerResponse = {
  statusType: StatusTypeEnum
  data: Data | Right<FailureResponse, SignInControllerSuccessResponse>
}
