import { SignIn } from '@domain/usecases/auth'
import { Logger } from '@infrastructure/adapters'
import { error, ok, serverError } from '@presenters/helpers'
import { Controller } from '@presenters/protocols/controller'
import { SignInEntryValidator } from '@presenters/validators/auth/signIn.validator'
import {
  EitherSignInControllerResponse,
  SignInControllerRequest,
} from '../types/signin.type'

export class SignInController implements Controller {
  constructor(
    private readonly signInUsecase: SignIn,
    private readonly logger: Logger
  ) {}

  async handle(
    request: SignInControllerRequest
  ): Promise<EitherSignInControllerResponse> {
    try {
      const dataOrError = SignInEntryValidator.create(request)

      if (dataOrError.isLeft()) {
        return error(dataOrError?.value?.errorType, dataOrError?.value?.message)
      }

      const data = dataOrError

      const result = await this.signInUsecase.handle(data.value)

      if (result.isLeft()) {
        return error(result?.value?.errorType, result?.value?.message)
      }

      return ok(result.value)
    } catch (error) {
      this.logger.error(`[X] SignInController.handle: ${error}`)
      return serverError()
    }
  }
}
