import { CreateUserAccount } from '@domain/usecases/user'
import { Logger } from '@infrastructure/adapters'
import { error, noContent, serverError } from '@presenters/helpers'
import { Controller } from '@presenters/protocols/controller'
import { CreateUserAccountEntryValidator } from '@presenters/validators/user'
import {
  CreateUserAccountControllerRequest,
  EitherCreateUserAccountResponse,
} from '../types'

export class CreateUserAccountController implements Controller {
  constructor(
    private readonly createUserAccount: CreateUserAccount,
    private readonly logger: Logger
  ) {}

  async handle(
    request: CreateUserAccountControllerRequest
  ): Promise<EitherCreateUserAccountResponse> {
    try {
      const dataOrError = CreateUserAccountEntryValidator.validate(request)

      if (dataOrError.isLeft()) {
        return error(dataOrError?.value?.errorType, dataOrError?.value?.message)
      }

      const data = dataOrError

      const result = await this.createUserAccount.handle(data.value)

      if (result.isLeft()) {
        return error(result?.value?.errorType, result?.value?.message)
      }

      return noContent()
    } catch (error) {
      this.logger.error(`[X] CreateUserAccountController.handle: ${error}`)
      return serverError()
    }
  }
}
