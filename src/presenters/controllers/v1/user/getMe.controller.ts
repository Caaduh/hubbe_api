import { GetLoggedUser } from '@domain/usecases/user'
import { Logger } from '@infrastructure/adapters'
import { error, ok, serverError } from '@presenters/helpers'
import { Controller } from '@presenters/protocols/controller'
import { GetLoggedUserEntryValidator } from '@presenters/validators/user'
import {
  EitherGetLoggedUserControllerResponse,
  GetLoggedUserControllerRequest,
} from '../types'

export class GetLoggedUserController implements Controller {
  constructor(
    private readonly getLoggedUserAccount: GetLoggedUser,
    private readonly logger: Logger
  ) {}

  async handle(
    request: GetLoggedUserControllerRequest
  ): Promise<EitherGetLoggedUserControllerResponse> {
    try {
      const dataOrError = GetLoggedUserEntryValidator.validate(request)

      if (dataOrError.isLeft()) {
        return error(dataOrError?.value?.errorType, dataOrError?.value?.message)
      }

      const data = dataOrError

      const result = await this.getLoggedUserAccount.handle(data.value)

      if (result.isLeft()) {
        return error(result?.value?.errorType, result?.value?.message)
      }

      return ok(result.value)
    } catch (error) {
      this.logger.error(`[X] GetLoggedUserController.handle: ${error}`)
      return serverError()
    }
  }
}
