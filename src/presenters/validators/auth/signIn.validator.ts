import { Either, left, right } from '@domain/either'
import { SignInControllerRequest } from '@presenters/controllers/v1/types/signin.type'
import { RequiredParametersError } from '@presenters/errors/requiredParametersError.error'

type PossibleErrors = RequiredParametersError

type Request = SignInControllerRequest
type Response = SignInControllerRequest

export class SignInEntryValidator {
  public static create(request: Request): Either<PossibleErrors, Response> {
    const { email, password } = request

    if (!email || !password) {
      return left(
        new RequiredParametersError('email and password are required fields')
      )
    }

    return right({
      email: request.email,
      password: request.password,
    })
  }
}
