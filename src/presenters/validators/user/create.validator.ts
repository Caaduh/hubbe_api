import { Either, left, right } from '@domain/either'
import { CreateUserAccountControllerRequest } from '@presenters/controllers/v1/types'
import { RequiredParametersError } from '@presenters/errors/requiredParametersError.error'

type PossibleErrors = RequiredParametersError

type Request = CreateUserAccountControllerRequest
type Response = CreateUserAccountControllerRequest

export class CreateUserAccountEntryValidator {
  public static validate(request: Request): Either<PossibleErrors, Response> {
    if (!request.fullName) {
      return left(new RequiredParametersError('fullName field are required'))
    }

    if (!request.email) {
      return left(new RequiredParametersError('email field are required'))
    }

    if (!request.password) {
      return left(new RequiredParametersError('password field are required'))
    }

    return right({
      fullName: request.fullName,
      email: request.email,
      password: request.password,
    })
  }
}
