import { Either, left, right } from '@domain/either'
import { GetLoggedUserControllerRequest } from '@presenters/controllers/v1/types'

import { RequiredParametersError } from '@presenters/errors/requiredParametersError.error'

type PossibleErrors = RequiredParametersError

type Request = GetLoggedUserControllerRequest
type Response = GetLoggedUserControllerRequest

export class GetLoggedUserEntryValidator {
  public static validate(request: Request): Either<PossibleErrors, Response> {
    if (!request.authorization || !request.userId) {
      return left(
        new RequiredParametersError('authorization field are required')
      )
    }

    return right({
      authorization: request.authorization,
      userId: request.userId,
    })
  }
}
