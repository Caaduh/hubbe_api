import { Either, left, right } from '@domain/either'
import { WebsocketEventsEnum } from '@domain/enums/websocketEvents.enum'
import { RequiredParametersError } from '@presenters/errors/requiredParametersError.error'

type RequestWebsocket = {
  data: { authorization: string }
  event: WebsocketEventsEnum
}

export class WebsocketEntryValidator {
  public static validate(
    request: RequestWebsocket
  ): Either<RequiredParametersError, RequestWebsocket> {
    if (!request?.data?.authorization) {
      return left(
        new RequiredParametersError(
          'Authorization is required, connection closed'
        )
      )
    }

    if (!request?.event) {
      return left(new RequiredParametersError('Event is required'))
    }

    const acceptedEvents = [
      WebsocketEventsEnum.USER_EXITED_ON_SAFE_PAGE,
      WebsocketEventsEnum.USER_LOGGED_ON_SAFE_PAGE,
      WebsocketEventsEnum.VERIFY_SAFE_PAGE_STATUS,
    ]

    if (!acceptedEvents.includes(request.event)) {
      return left(new RequiredParametersError('Event type wrong'))
    }

    return right({
      event: request.event,
      data: {
        authorization: request.data.authorization,
      },
    })
  }
}
