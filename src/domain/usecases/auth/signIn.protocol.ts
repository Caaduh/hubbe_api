import { InvalidLoginError, UserNotFoundError } from '@application/errors'
import { Either } from '@domain/either'

export type SignInInput = {
  email: string
  password: string
}

type SuccessOutput = {
  accessToken: string
  refreshToken: string
}

type PossibleErrors = UserNotFoundError | InvalidLoginError

export type EitherSignInOutput = Either<PossibleErrors, SuccessOutput>

export interface SignIn {
  handle(input: SignInInput): Promise<EitherSignInOutput>
}
