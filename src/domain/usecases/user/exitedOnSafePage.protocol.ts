import { InvalidTokenError } from '@application/errors'
import { Either } from '@domain/either'

export type UserExitedOnSafePageUsecaseInput = {
  authorization: string
}

export type EitherUserExitedOnSafePageUsecaseOutput = Either<
  InvalidTokenError,
  string
>

export interface UserExitedOnSafePage {
  handle(
    input: UserExitedOnSafePageUsecaseInput
  ): Promise<EitherUserExitedOnSafePageUsecaseOutput>
}
