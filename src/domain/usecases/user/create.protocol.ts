import { UserNotFoundError } from '@application/errors'
import { Either } from '@domain/either'
import { InvalidEmailError } from '@domain/errors'

type PossibleErrors = UserNotFoundError | InvalidEmailError

export type EitherCreateUserAccountOutput = Either<PossibleErrors, boolean>

export type CreateUserAccountUsecaseInput = {
  fullName: string
  email: string
  password: string
}

export interface CreateUserAccount {
  handle(
    input: CreateUserAccountUsecaseInput
  ): Promise<EitherCreateUserAccountOutput>
}
