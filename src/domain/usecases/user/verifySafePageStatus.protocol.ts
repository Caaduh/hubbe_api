import { InvalidTokenError } from '@application/errors'
import { Either } from '@domain/either'

export type VerifySafePageStatusUsecaseInput = {
  authorization: string
}

export type EitherVerifySafePageStatusUsecaseOutput = Either<
  InvalidTokenError,
  string
>

export interface VerifySafePageStatus {
  handle(
    input: VerifySafePageStatusUsecaseInput
  ): Promise<EitherVerifySafePageStatusUsecaseOutput>
}
