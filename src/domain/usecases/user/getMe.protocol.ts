import { UserNotFoundError } from '@application/errors'
import { Either } from '@domain/either'
import { InvalidEmailError } from '@domain/errors'
import { BaseInputData } from '@domain/types/baseInputData.protocol'

type PossibleErrors = UserNotFoundError | InvalidEmailError

type SuccessOutput = { fullName: string; email: string }

export type EitherGetLoggedUserOutput = Either<PossibleErrors, SuccessOutput>

export type GetLoggedUserUsecaseInput = BaseInputData

export interface GetLoggedUser {
  handle(input: GetLoggedUserUsecaseInput): Promise<EitherGetLoggedUserOutput>
}
