import {
  InvalidTokenError,
  NotLoggedOnSafePageError,
  SafePageBusyError,
} from '@application/errors'
import { Either } from '@domain/either'

type PossibleErrors =
  | InvalidTokenError
  | SafePageBusyError
  | NotLoggedOnSafePageError

export type EitherUserLoggedOnSafePageUsecaseOutput = Either<
  PossibleErrors,
  string
>

export type UserLoggedOnSafePagePageUsecaseInput = {
  authorization: string
}

export interface UserLoggedOnSafePagePage {
  handle(
    input: UserLoggedOnSafePagePageUsecaseInput
  ): Promise<EitherUserLoggedOnSafePageUsecaseOutput>
}
