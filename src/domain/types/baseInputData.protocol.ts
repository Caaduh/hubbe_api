export type BaseInputData = {
  authorization: string
  userId: string
}
