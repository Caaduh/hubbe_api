export enum WebsocketEventsEnum {
  USER_LOGGED_ON_SAFE_PAGE = 'userLoggedOnSafePage',
  USER_EXITED_ON_SAFE_PAGE = 'userExitedOnSafePage',
  VERIFY_SAFE_PAGE_STATUS = 'verifySafePageStatus',
  CONNECTED_SAFE_PAGE = 'ConnectedSafePage',
  SAFE_PAGE_RELEASED = 'SafePageReleased',
}

export enum WebsocketEventsToNotifyClientsEnum {
  SAFE_PAGE_FREE = 'SafePageFree',
  SAFE_PAGE_BUSY = 'SafePageBusy',
}
