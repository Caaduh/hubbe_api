import { Base } from './base'

export class LoggedUser extends Base {
  constructor(
    public readonly user: number,
    public readonly isLogged: boolean,
    public readonly whichPage: string
  ) {
    super()
  }

  static accessSafePage(user: number): LoggedUser {
    return new LoggedUser(user, true, 'SAFE_PAGE')
  }

  static exitSafePage(user: number): LoggedUser {
    return new LoggedUser(user, false, 'SAFE_PAGE')
  }

  static IamInSafePage(user: number): LoggedUser {
    return new LoggedUser(user, true, 'SAFE_PAGE')
  }
}
