export type CreateUserOutput = {
  fullName: string
  email: string
  password: string
}
