export class Base {
  public id: number
  public createdAt: Date
  public modifiedAt: Date
}
