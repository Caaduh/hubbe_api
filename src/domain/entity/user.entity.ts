import { Either, left, right } from '@domain/either'
import {
  InvalidEmailError,
  InvalidNameError,
  InvalidPasswordError,
} from '@domain/errors'
import { CreateUserAccountUsecaseInput } from '@domain/usecases/user'
import { Base } from './base'
import { Email } from './email.entity'

type CreatePossibleErrors = InvalidEmailError | InvalidNameError
type SignInPossibleErrors = InvalidEmailError

export class User extends Base {
  constructor(
    public readonly fullName: string,
    public readonly email: string,
    public password: string
  ) {
    super()
  }

  public static create(
    input: CreateUserAccountUsecaseInput
  ): Either<CreatePossibleErrors, User> {
    const isInvalidUserEmail = Email.isInvalid(input.email)

    if (isInvalidUserEmail) {
      return left(new InvalidEmailError(input.email))
    }

    const fullNameIsInvalid = this.nameIsInvalid(input.fullName)

    if (fullNameIsInvalid) {
      return left(new InvalidNameError())
    }

    const invalidPassword = this.passwordIsInvalid(input.password)

    if (invalidPassword.isLeft()) {
      return left(invalidPassword.value)
    }

    return right(new User(input.fullName, input.email, input.password))
  }

  public static signIn(
    email: string,
    password: string
  ): Either<SignInPossibleErrors, { email: string; password: string }> {
    const isInvalidUserEmail = Email.isInvalid(email)

    if (isInvalidUserEmail) {
      return left(new InvalidEmailError(email))
    }

    return right({
      email,
      password,
    })
  }

  private static nameIsInvalid(fullName: string): boolean {
    const regex = /^\w+\s+\w+$/

    return !regex.test(fullName)
  }

  public static passwordIsInvalid(
    password: string
  ): Either<InvalidPasswordError, boolean> {
    if (!password) {
      return left(new InvalidPasswordError())
    }

    const regexPasswordValidator =
      /^(?=.*[A-Z])(?=.*\d)(?=.*[^A-Za-z0-9]).{8,}$/

    const passwordIsInvalid = !regexPasswordValidator.test(password)

    if (passwordIsInvalid) {
      return left(new InvalidPasswordError())
    }

    return right(false)
  }
}
