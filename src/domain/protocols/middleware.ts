export interface Middleware {
  handle: (httpRequest: any) => Promise<any>
}
