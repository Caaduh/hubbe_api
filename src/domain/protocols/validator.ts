export interface Validator<I = any, O = any> {
  validate(input: I): O
}
