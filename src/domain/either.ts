export type Either<L, R> = Left<L, R> | Right<L, R>

export class Left<L, R> {
  constructor(public readonly value: L) {}

  public isLeft(): this is Left<L, R> {
    return true
  }

  public isRight(): this is Right<L, R> {
    return false
  }
}

export class Right<L, R> {
  constructor(public readonly value: R) {}

  public isLeft(): this is Left<L, R> {
    return false
  }

  public isRight(): this is Right<L, R> {
    return true
  }
}

export const left = <L, R>(left: L): Either<L, R> => {
  return new Left(left)
}

export const right = <L, R>(right: R): Either<L, R> => {
  return new Right(right)
}
