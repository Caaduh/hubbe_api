import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class EmailNotAllowedError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor() {
    const m = 'This email not allowed in the system'
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.EMAIL_TYPE_NOT_ALLOWED
  }
}
