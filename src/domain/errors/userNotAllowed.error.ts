import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class NotAllowedUserError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor() {
    const m = 'This action is not permitted for you'
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.USER_NOT_ALLOWED
  }
}
