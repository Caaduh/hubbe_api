import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class InvalidPasswordError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor() {
    const m = 'The password is invalid'
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.INVALID_PASSWORD
  }
}
