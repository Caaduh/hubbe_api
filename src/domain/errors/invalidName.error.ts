import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class InvalidNameError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor() {
    const m = 'Name must be first name and last name'
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.INVALID_NAME
  }
}
