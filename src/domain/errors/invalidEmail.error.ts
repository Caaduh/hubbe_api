import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class InvalidEmailError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor(email: string) {
    const m = `The email ${email} is invalid`
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.INVALID_EMAIL
  }
}
