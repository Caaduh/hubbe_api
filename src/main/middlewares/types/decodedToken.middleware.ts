export type DecodeTokenMiddlewareInput = {
  authorization: string
}

export type DecodeTokenMiddlewareOutput = {
  userId: number
}
