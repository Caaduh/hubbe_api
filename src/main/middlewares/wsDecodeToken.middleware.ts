import { Middleware } from '@domain/protocols/middleware'
import { JwtAdapterProtocol } from '@main/adapters'
import { forbidden, ok, serverError } from '@presenters/helpers'
import {
  DecodeTokenMiddlewareInput,
  DecodeTokenMiddlewareOutput,
} from './types'

export class WebsocketDecodeTokenMiddleware implements Middleware {
  constructor(private readonly jwtAdapter: JwtAdapterProtocol) {}

  public async handle({
    authorization,
  }: DecodeTokenMiddlewareInput): Promise<DecodeTokenMiddlewareOutput> {
    if (!authorization) return forbidden('authorization required field')
    try {
      const tokenWithoutBearer = authorization.replace('Bearer ', '')

      const verifySignatureToken = this.jwtAdapter.verify(tokenWithoutBearer)

      if (!verifySignatureToken)
        return forbidden('authorization is invalid or expired')

      const decoded = await this.jwtAdapter.decrypt(tokenWithoutBearer)

      if (!decoded || !decoded?.userId)
        return forbidden('authorization is invalid')

      return ok({
        userId: decoded.userId,
      })
    } catch (error) {
      console.log(error)
      return serverError()
    }
  }
}
