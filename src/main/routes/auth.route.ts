import { Router } from 'express'

import { expressRouterAdapter } from '@main/adapters/expressRouter.adapter'
import { SignInController } from '@presenters/controllers/v1/auth'

export class AuthRoute {
  private routeV1 = '/api/v1/auth'

  constructor(private readonly signInController: SignInController) {}

  register(router: Router): void {
    router.post(
      `${this.routeV1}/signin`,
      expressRouterAdapter(
        this.signInController.handle.bind(this.signInController)
      )
    )
  }
}
