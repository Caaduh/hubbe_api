import { swaggerConfig } from '@presenters/docs'
import { Express } from 'express'
import { serveWithOptions, setup, SwaggerOptions } from 'swagger-ui-express'

export class DocRoute {
  private routeV1 = '/api/v1/docs'
  private options: SwaggerOptions = {
    customCssUrl: '../../../public/custom.css',
    customJs: ['../../../public/standalone.js', '../../../public/bundle.js'],
  }

  register(app: Express): void {
    app.use(
      this.routeV1,
      serveWithOptions({ redirect: true }),
      setup(swaggerConfig, this.options)
    )
  }
}
