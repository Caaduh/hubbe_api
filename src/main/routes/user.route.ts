import { Router } from 'express'

import { expressRouterAdapter } from '@main/adapters/expressRouter.adapter'
import { CreateUserAccountController } from '@presenters/controllers/v1/user/create.controller'
import { GetLoggedUserController } from '@presenters/controllers/v1/user/getMe.controller'
import { expressDecodeTokenMiddleware } from '@presenters/middlewares/expressDecodeToken.middleware'

export class UserRoute {
  private routeV1 = '/api/v1/user'

  constructor(
    private readonly createUserAccountController: CreateUserAccountController,
    private readonly getUserLoggedController: GetLoggedUserController
  ) {}

  register(router: Router): void {
    router.get(
      `${this.routeV1}/me`,
      expressDecodeTokenMiddleware,
      expressRouterAdapter(
        this.getUserLoggedController.handle.bind(this.getUserLoggedController)
      )
    )

    router.post(
      this.routeV1,
      expressRouterAdapter(
        this.createUserAccountController.handle.bind(
          this.createUserAccountController
        )
      )
    )
  }
}
