import { UserExitedOnSafePageUsecase } from '@application/usecases/user/exitedOnSafePage.usecase'
import { makeLoggedUserRepository } from '@main/factories/infrastructure/repositories/loggedUser.factory'
import { makeJwtAdapter } from '@main/factories/main/adapters/jwt.factory'

export function makeUserExitedOnSafePageUsecase(): UserExitedOnSafePageUsecase {
  return new UserExitedOnSafePageUsecase(
    makeLoggedUserRepository(),
    makeJwtAdapter()
  )
}
