import { UserLoggedOnSafePageUsecase } from '@application/usecases/user/loggedOnSafePage.usecase'
import { makeLoggedUserRepository } from '@main/factories/infrastructure/repositories/loggedUser.factory'
import { makeJwtAdapter } from '@main/factories/main/adapters/jwt.factory'

export function makeUserLoggedOnSafePageUsecase(): UserLoggedOnSafePageUsecase {
  return new UserLoggedOnSafePageUsecase(
    makeLoggedUserRepository(),
    makeJwtAdapter()
  )
}
