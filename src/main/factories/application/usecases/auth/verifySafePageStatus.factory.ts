import { VerifySafePageStatusUsecase } from '@application/usecases/user/verifySafePageStatus.usecase'
import { makeLoggedUserRepository } from '@main/factories/infrastructure/repositories/loggedUser.factory'
import { makeJwtAdapter } from '@main/factories/main/adapters/jwt.factory'

export function makeVerifyOnSafePageStatusUsecase(): VerifySafePageStatusUsecase {
  return new VerifySafePageStatusUsecase(
    makeLoggedUserRepository(),
    makeJwtAdapter()
  )
}
