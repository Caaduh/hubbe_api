import { SignInUsecase } from '@application/usecases/auth'
import { makeUserRepository } from '@main/factories/infrastructure/repositories/user.factory'
import { makeBcryptAdapter } from '@main/factories/main/adapters/bcrypt.factory'
import { makeJwtAdapter } from '@main/factories/main/adapters/jwt.factory'

export function makeSignInUsecase(): SignInUsecase {
  return new SignInUsecase(
    makeUserRepository(),
    makeBcryptAdapter(),
    makeJwtAdapter()
  )
}
