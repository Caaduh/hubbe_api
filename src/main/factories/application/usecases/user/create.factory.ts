import { CreateUserAccountUsecase } from '@application/usecases/user'
import { makeUserRepository } from '@main/factories/infrastructure/repositories/user.factory'
import { makeBcryptAdapter } from '@main/factories/main/adapters/bcrypt.factory'

export function makeCreateUserUsecase(): CreateUserAccountUsecase {
  return new CreateUserAccountUsecase(makeUserRepository(), makeBcryptAdapter())
}
