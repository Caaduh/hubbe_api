import { GetLoggedUserUsecase } from '@application/usecases/user'
import { makeUserRepository } from '@main/factories/infrastructure/repositories/user.factory'

export function makeGetLoggedUserUsecase(): GetLoggedUserUsecase {
  return new GetLoggedUserUsecase(makeUserRepository())
}
