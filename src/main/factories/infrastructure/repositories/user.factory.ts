import { UserRepository } from '@infrastructure/repositories'
import { makePostgresqlAdapter } from '@main/factories/main/adapters/postgres.factory'
import { makeWinstonLoggerAdapter } from '@main/factories/main/adapters/winston.factory'

export function makeUserRepository(): UserRepository {
  return new UserRepository(makePostgresqlAdapter(), makeWinstonLoggerAdapter())
}
