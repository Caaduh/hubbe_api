import { LoggedUserRepository } from '@infrastructure/repositories/loggedUser.repository'
import { makePostgresqlAdapter } from '@main/factories/main/adapters/postgres.factory'
import { makeWinstonLoggerAdapter } from '@main/factories/main/adapters/winston.factory'

export function makeLoggedUserRepository(): LoggedUserRepository {
  return new LoggedUserRepository(
    makePostgresqlAdapter(),
    makeWinstonLoggerAdapter()
  )
}
