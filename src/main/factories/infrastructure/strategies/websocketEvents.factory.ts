import { WebsocketEventsStrategy } from '@infrastructure/strategies/websocketEvents.strategy'

export const makeWebsocketEventsStrategy = provider => {
  return new WebsocketEventsStrategy(provider)
}
