import { makeSignInUsecase } from '@main/factories/application/usecases/auth/signIn.factory'
import { makeWinstonLoggerAdapter } from '@main/factories/main/adapters/winston.factory'
import { SignInController } from '@presenters/controllers/v1/auth'

export function makeSignInController(): SignInController {
  return new SignInController(makeSignInUsecase(), makeWinstonLoggerAdapter())
}
