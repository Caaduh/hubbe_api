import { makeGetLoggedUserUsecase } from '@main/factories/application/usecases/user/getMe.factory'
import { makeWinstonLoggerAdapter } from '@main/factories/main/adapters/winston.factory'
import { GetLoggedUserController } from '@presenters/controllers/v1/user/getMe.controller'

export function makeGetUserLoggedController(): GetLoggedUserController {
  return new GetLoggedUserController(
    makeGetLoggedUserUsecase(),
    makeWinstonLoggerAdapter()
  )
}
