import { makeCreateUserUsecase } from '@main/factories/application/usecases/user/create.factory'
import { makeWinstonLoggerAdapter } from '@main/factories/main/adapters/winston.factory'
import { CreateUserAccountController } from '@presenters/controllers/v1/user/create.controller'

export function makeCreateUserController(): CreateUserAccountController {
  return new CreateUserAccountController(
    makeCreateUserUsecase(),
    makeWinstonLoggerAdapter()
  )
}
