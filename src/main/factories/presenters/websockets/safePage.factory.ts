import { makeWinstonLoggerAdapter } from '@main/factories/main/adapters/winston.factory'
import { makeWebsocketAdapter } from '@main/factories/main/adapters/ws.factory'
import { WebSocket } from '@presenters/socket'

export const makeWebsocketEntrypoint = (): WebSocket => {
  return new WebSocket(makeWebsocketAdapter(), makeWinstonLoggerAdapter())
}
