import { AdminGetAllCompaniesEntryValidator } from '@presenters/validators/admin/getAllCompanies.validator'

export function makeGetAllCompaniesByAdminValidator(): AdminGetAllCompaniesEntryValidator {
  return new AdminGetAllCompaniesEntryValidator()
}
