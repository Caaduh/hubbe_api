import { AdminGetCompanyByIdEntryValidator } from '@presenters/validators/admin'

export function makeGetCompanyByAdminValidator(): AdminGetCompanyByIdEntryValidator {
  return new AdminGetCompanyByIdEntryValidator()
}
