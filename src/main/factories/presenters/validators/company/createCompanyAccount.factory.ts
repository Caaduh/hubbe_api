import { CreateCompanyEntryValidator } from '@presenters/validators/admin'

export function makeCreateCompanyAccountValidator(): CreateCompanyEntryValidator {
  return new CreateCompanyEntryValidator()
}
