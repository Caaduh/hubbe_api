import { JsonWebTokenProvider } from '@main/providers/jsonwebtoken.provider'

export function makeJsonwebtokenProvider(): JsonWebTokenProvider {
  return new JsonWebTokenProvider()
}
