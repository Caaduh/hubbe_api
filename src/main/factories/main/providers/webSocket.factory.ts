import { env } from '@main/env'
import { WebSocketProvider } from '@main/providers/ws.provider'

export function makeWebSocketProvider(): WebSocketProvider {
  return new WebSocketProvider({
    host: env.socket.host,
    port: env.socket.port,
  })
}
