import { PostgresqlAdapter } from '@main/adapters'

export function makePostgresqlAdapter(): PostgresqlAdapter {
  return new PostgresqlAdapter()
}
