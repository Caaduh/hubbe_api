import { WebsocketAdapter } from '@main/adapters/ws.adapter'
import { makeWebSocketProvider } from '../providers/webSocket.factory'

export function makeWebsocketAdapter(): WebsocketAdapter {
  return new WebsocketAdapter(makeWebSocketProvider())
}
