import { JwtAdapter } from '@main/adapters'
import { env } from '@main/env'
import { makeJsonwebtokenProvider } from '../providers/jsonwebtoken.factory'

export function makeJwtAdapter(): JwtAdapter {
  return new JwtAdapter(makeJsonwebtokenProvider(), env.jwt.secret)
}
