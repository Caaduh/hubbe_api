import { BcryptAdapter } from '@main/adapters'

export function makeBcryptAdapter(): BcryptAdapter {
  return new BcryptAdapter()
}
