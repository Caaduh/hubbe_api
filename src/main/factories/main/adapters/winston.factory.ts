import { WinstonLoggerAdapter } from '@main/adapters/logger.adapter'

export function makeWinstonLoggerAdapter(): WinstonLoggerAdapter {
  return new WinstonLoggerAdapter()
}
