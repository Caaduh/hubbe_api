import { DecodeTokenMiddleware } from '@main/middlewares/expressDecodeToken.middleware'
import { makeJwtAdapter } from '../adapters/jwt.factory'

export const makeExpressDecodeTokenMiddleware = (): DecodeTokenMiddleware => {
  return new DecodeTokenMiddleware(makeJwtAdapter())
}
