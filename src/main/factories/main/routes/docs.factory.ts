import { DocRoute } from '@main/routes'

export function makeDocsRoute(): DocRoute {
  return new DocRoute()
}
