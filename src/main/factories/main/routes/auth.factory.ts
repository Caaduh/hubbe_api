import { makeSignInController } from '@main/factories/presenters/controllers/v1/auth/signIn.factory'
import { AuthRoute } from '@main/routes/auth.route'

export function makeAuthRoute(): AuthRoute {
  const controller: any = makeSignInController()
  return new AuthRoute(controller)
}
