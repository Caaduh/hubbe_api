import { makeCreateUserController } from '@main/factories/presenters/controllers/v1/user/create.factory'
import { makeGetUserLoggedController } from '@main/factories/presenters/controllers/v1/user/getMe.factory'
import { UserRoute } from '@main/routes'

export function makeUserRoute(): UserRoute {
  return new UserRoute(
    makeCreateUserController(),
    makeGetUserLoggedController()
  )
}
