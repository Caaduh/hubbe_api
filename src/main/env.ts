import * as dotenv from 'dotenv'
import { Env } from './env.type'

dotenv.config()

export const env: Env = {
  httpPort: (process.env.PORT && parseInt(process.env.PORT, 10)) || 3000,
  environment: process.env.NODE_ENV || 'develop',
  db: {
    dbPort: parseInt(process.env.DB_PORT || '5432', 10),
    dbConnector: process.env.DB_CONNECTOR || 'postgresql',
    dbHost: process.env.DB_HOST || 'localhost',
    dbUsername: process.env.DB_USERNAME || 'postgres',
    dbPassword: process.env.DB_PASSWORD || 'eu@hubbe',
    dbDatabase:
      process.env.NODE_ENV === 'testing'
        ? 'hubbe_test'
        : process.env.DB_DATABASE || 'postgres',
    dbPoolMin: parseInt(process.env.DB_POOL_MIN || '05'),
    dbPoolMax: parseInt(process.env.DB_POOL_MAX || '150'),
    dbDebug: process.env.DB_DEBUG === 'true',
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    accessTokenExpire: process.env.AUTH_TOKEN_EXPIRES_IN_MINUTES,
    refreshTokenExpire: process.env.AUTH_REFRESH_TOKEN_EXPIRES_IN_DAYS,
  },
  socket: {
    port: parseInt(process.env.WEBSOCKET_PORT) || 3333,
    host: process.env.WEBSOCKET_HOST,
  },
}
