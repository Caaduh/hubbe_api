import WebSocket, { ServerOptions, Server as WebSocketServer } from 'ws'

export interface WSProtocol {
  getConnection(): WebSocket.Server
  closeConnection(): void
}

export class WebSocketProvider implements WSProtocol {
  private wss: WebSocketServer

  constructor(server: ServerOptions) {
    this.wss = new WebSocketServer({ host: server?.host, port: server.port })
  }

  public getConnection(): WebSocket.Server {
    return this.wss
  }

  public closeConnection(): void {
    this.wss.close()
  }
}
