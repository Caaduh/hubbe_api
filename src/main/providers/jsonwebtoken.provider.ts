import jwt from 'jsonwebtoken'
import jwtDecode, { JwtPayload } from 'jwt-decode'

export class JsonWebTokenProvider {
  verify(token: string, secretKey: string): string | jwt.JwtPayload {
    const decoded = jwt.verify(token, secretKey)

    return decoded
  }
  encrypt(secretKey: string, expiresIn: string, payload: any): string {
    const options: jwt.SignOptions = {
      expiresIn: expiresIn,
    }
    const token = jwt.sign(payload, secretKey, options)

    return token
  }

  decode(tokenWithoutBearer: string): JwtPayload {
    return jwtDecode<JwtPayload>(tokenWithoutBearer)
  }
}
