import { InitApplication } from '@main/app'
import { env } from '@main/env'
import { cors } from '@main/middlewares'
import cluster, { Worker } from 'cluster'
import os from 'os'

const startApplication = async () => {
  const init = new InitApplication()

  init.app.use(cors)

  init.app.listen(env.httpPort, () => {
    console.log(
      '\x1b[32m',
      `[API] Server running at http://localhost:${env.httpPort}`
    )
  })

  init.socket.connect()
}

if (env.environment !== 'develop') {
  if (cluster.isPrimary) {
    const processesCount = os.cpus().length
    console.log(`Primary ${process.pid} is running`)
    startApplication()

    for (let index = 0; index < processesCount; index++) {
      cluster.fork()
    }

    cluster.on('exit', (worker: Worker, code: number) => {
      if (code !== 0 && !worker.exitedAfterDisconnect) {
        console.log(
          `Worker ${worker.process.pid} died... scheduling another one!`
        )

        cluster.fork()
        startApplication()
      }
    })
  }
} else {
  startApplication()
}
