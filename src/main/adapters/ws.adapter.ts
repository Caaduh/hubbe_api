import { Socket } from '@infrastructure/adapters/socket.protocol'
import { WSProtocol } from '@main/providers/ws.provider'

export class WebsocketAdapter implements Socket {
  constructor(private readonly ws: WSProtocol) {}

  getConnection() {
    return this.ws.getConnection()
  }

  closeConnection() {
    this.ws.closeConnection()
  }
}
