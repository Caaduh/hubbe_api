import { formattedTimestamp } from '@application/helpers/formattedTimeStampToLogger.helper'
import { Logger } from '@infrastructure/adapters/logger/logger.protocol'
import path from 'path'
import { Logger as WLogger, createLogger, format, transports } from 'winston'

const { combine, timestamp, label, printf } = format

export class WinstonLoggerAdapter implements Logger {
  protected log: WLogger

  constructor() {
    const myFormat = printf(({ level, message, label, timestamp }) => {
      const formatDateToPtBr = formattedTimestamp(timestamp)

      return `[${level}]: ${formatDateToPtBr} - ${message} - ${label}`
    })

    const stackTrace = new Error().stack.split('\n')

    let callingFile = ''
    for (let i = 3; i < stackTrace.length; i++) {
      const match = stackTrace[i].match(/\s+at\s(.+)\s\((.+):(\d+):(\d+)\)/)
      if (match && match[3] !== __filename) {
        callingFile = match[1]
        break
      }
    }

    const logger = createLogger({
      format: combine(
        format.colorize({ all: true }),
        label({ label: path.basename(callingFile) }),
        timestamp(),
        myFormat
      ),
      defaultMeta: { service: 'user-service' },
      transports: [
        new transports.File({
          filename: 'logs/error.log',
          level: 'error',
        }),
        new transports.File({
          filename: 'logs/warn.log',
          level: 'warn',
        }),
      ],
    })

    if (process.env.NODE_ENV !== 'production') {
      logger.add(
        new transports.Console({
          format: combine(
            format.colorize({ all: true }),
            label({ label: path.basename(callingFile) }),
            timestamp(),
            myFormat
          ),
        })
      )
    }

    this.log = logger
  }

  info(message: string): void {
    this.log.info(message)
  }
  warn(message: string): void {
    this.log.warn(message)
  }
  error(message: string): void {
    this.log.error(message)
  }
  debug(message: string): void {
    this.log.debug(message)
  }
}
