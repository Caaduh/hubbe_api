import { ConvertErrorStatusTypeOnStatusCode } from '@infrastructure/adapters/convertStatusTypeOnStatusCode.protocol'
import { StatusTypeEnum } from '@presenters/enums/errors.enum'

const badRequestExample = [
  StatusTypeEnum.REQUIRED_PARAMETERS_ERROR,
  StatusTypeEnum.INVALID_EMAIL,
  StatusTypeEnum.EMAIL_TYPE_NOT_ALLOWED,
  StatusTypeEnum.INVALID_PASSWORD,
  StatusTypeEnum.INVALID_LOGIN,
  StatusTypeEnum.INVALID_NAME,
]
const conflictErrorExample = [StatusTypeEnum.USER_EXISTS_ERROR]
const notFoundErrorExample = [StatusTypeEnum.USER_NOT_FOUND]
const forbiddenErrorExample = [StatusTypeEnum.USER_NOT_ALLOWED]
const processErrorExample = [StatusTypeEnum.PROCESS_ENTITY_ERROR]

const serverErrorExample = [StatusTypeEnum.SERVER_ERROR]

export class ExpressConvertStatusTypeOnStatusCodeAdapter
  implements ConvertErrorStatusTypeOnStatusCode
{
  execute(statusType: StatusTypeEnum): number {
    if (badRequestExample.includes(statusType)) {
      return 400
    }

    if (forbiddenErrorExample.includes(statusType)) {
      return 403
    }

    if (notFoundErrorExample.includes(statusType)) {
      return 404
    }

    if (conflictErrorExample.includes(statusType)) {
      return 409
    }

    if (processErrorExample.includes(statusType)) {
      return 422
    }

    if (serverErrorExample.includes(statusType)) {
      return 500
    }
  }
}
