import { CompareProtocol, HashProtocol } from '@infrastructure/adapters'
import bcrypt from 'bcryptjs'

export class BcryptAdapter implements HashProtocol, CompareProtocol {
  public async generate(plainText: string, salt = 12): Promise<string> {
    const hashedInformation = bcrypt.hashSync(plainText, salt)

    return hashedInformation
  }

  public async compare(
    plainText: string,
    hashedInformation: string
  ): Promise<boolean> {
    const informationComparedResult = bcrypt.compareSync(
      plainText,
      hashedInformation
    )

    return informationComparedResult
  }
}
