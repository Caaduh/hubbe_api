import { Knex } from 'knex'
import { KnexPostgis } from 'knex-postgis'

import database, { databasePostgis } from '@infrastructure/database'
import {
  IPostgresqlAdapter,
  PostgisDatabase,
  PostgresqlAdapterConfig,
  PostgresqlDatabase,
} from '@infrastructure/infrastructure.type'

export class PostgresqlAdapter implements IPostgresqlAdapter {
  private table: string
  private database: PostgresqlDatabase
  private postgis: PostgisDatabase

  constructor(config?: PostgresqlAdapterConfig) {
    this.database = config?.dbConn || database()
    this.postgis = databasePostgis()
    this.table = ''
  }

  get dbPostgis(): KnexPostgis {
    return this.postgis
  }

  get db(): Knex.QueryBuilder {
    return this.database(this.table)
  }

  set tableName(name: string) {
    this.table = name
  }

  get dbInstance(): PostgresqlDatabase {
    return this.database
  }
}
