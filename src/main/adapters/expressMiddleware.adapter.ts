import { MiddlewareProtocol } from '@presenters/protocols/middleware'
import { NextFunction, Request, Response } from 'express'

export const expressMiddlewareAdapter =
  (middleware: MiddlewareProtocol, userId?: number) =>
  async (req: Request, res: Response, next: NextFunction) => {
    const { statusCode, data } = await middleware.handle({
      ...req.headers,
      id: userId ? req.params?.id || req.params?.id : undefined,
    })

    const validEntries = Object.entries(data).filter(([, value]) => value)
    res.locals = { ...res.locals, ...Object.fromEntries(validEntries) }

    if (statusCode >= 200 && statusCode <= 299) {
      next()
    } else {
      res.status(statusCode).json({
        data: { error: data?.error },
      })
    }
  }
