import { ExpressConvertStatusTypeOnStatusCodeAdapter } from '@main/adapters/expressErrorMapper.adapter'
import { Request, Response } from 'express'

export const expressRouterAdapter =
  (fn: (values?: any) => void | any) => async (req: Request, res: Response) => {
    try {
      const body = Array.isArray(req.body) ? { data: req.body } : req.body

      const { statusCode, statusType, data } = await fn({
        ...body,
        ...req.headers,
        ...req.params,
        ...req.query,
        ...res.locals,
      })

      const expressConvertErrorTypeOnStatusCode =
        new ExpressConvertStatusTypeOnStatusCodeAdapter()

      let status = 0
      if (statusType) {
        status = expressConvertErrorTypeOnStatusCode.execute(statusType)
      } else {
        status = statusCode
      }

      if (status >= 200 && status <= 299) {
        const isFiltered = data?.data && data?.currentPage
        if (isFiltered) {
          return res.status(statusCode).json(data)
        }
        return res.status(statusCode).json({ data })
      } else {
        return res.status(status).json({
          data: { error: data },
        })
      }
    } catch (e) {
      console.error(e)
      return res.status(500).json({
        error: 'Error',
      })
    }
  }
