import {
  JwtDecryptProtocol,
  JwtDecryptProtocolOutput,
} from '@infrastructure/adapters/jwt/decrypt.protocol'
import {
  JwtEncryptOutputProtocol,
  JwtEncryptProtocol,
} from '@infrastructure/adapters/jwt/encrypt.protocol'
import { JwtVerifyProtocol } from '@infrastructure/adapters/jwt/verify.protocol'
import { JsonWebTokenProvider } from '@main/providers/jsonwebtoken.provider'

import { env } from '@main/env'

export type JWtVerifyAdapterOutput = {
  userId: number
  iat: number
  exp: number
}

export interface JwtAdapterProtocol
  extends JwtEncryptProtocol,
    JwtDecryptProtocol,
    JwtVerifyProtocol {}

export class JwtAdapter
  implements JwtEncryptProtocol, JwtDecryptProtocol, JwtVerifyProtocol
{
  constructor(
    private readonly jwtProvider: JsonWebTokenProvider,
    private readonly secretKey: string
  ) {}
  verify(token: string): boolean | JWtVerifyAdapterOutput {
    try {
      const isCorrect: unknown = this.jwtProvider.verify(token, this.secretKey)
      return isCorrect as JWtVerifyAdapterOutput
    } catch (e) {
      return false
    }
  }
  encrypt(payload: any): JwtEncryptOutputProtocol {
    const accessToken = this.jwtProvider.encrypt(
      this.secretKey,
      env.jwt.accessTokenExpire,
      payload
    )

    const refreshToken = this.jwtProvider.encrypt(
      this.secretKey,
      env.jwt.refreshTokenExpire,
      payload
    )

    return { accessToken, refreshToken }
  }
  public decrypt(token: string): JwtDecryptProtocolOutput {
    const decoded: unknown = this.jwtProvider.decode(token)

    return decoded as JwtDecryptProtocolOutput
  }
}
