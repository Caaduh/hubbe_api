export type Env = {
  httpPort: number
  environment: string
  db: {
    dbPort: number
    dbConnector: string
    dbHost: string
    dbUsername: string
    dbPassword: string
    dbDatabase: string
    dbPoolMin: number
    dbPoolMax: number
    dbDebug: boolean
  }
  jwt: {
    secret: string
    accessTokenExpire: string
    refreshTokenExpire: string
  }
  socket: {
    port: number
    host: string
  }
}
