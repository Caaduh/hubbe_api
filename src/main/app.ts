import express from 'express'

import { WebsocketProtocol } from '@presenters/protocols/websocket'
import { HttpServer } from '../presenters/index'
import { WinstonLoggerAdapter } from './adapters/logger.adapter'
import { makeWinstonLoggerAdapter } from './factories/main/adapters/winston.factory'
import { makeWebsocketEntrypoint } from './factories/presenters/websockets/safePage.factory'

export class InitApplication {
  public app: express.Application
  public logger: WinstonLoggerAdapter
  public socket: WebsocketProtocol

  constructor() {
    const httpServer = new HttpServer()
    this.app = httpServer.httpApp()
    this.logger = makeWinstonLoggerAdapter()
    this.socket = makeWebsocketEntrypoint()
  }
}
