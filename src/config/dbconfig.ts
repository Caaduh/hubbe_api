import { env } from '../main/env'

const configConnection = () => {
  return {
    host: env.db.dbHost,
    port: env.db.dbPort,
    user: env.db.dbUsername,
    password: env.db.dbPassword,
    database: env.db.dbDatabase,
    supportBigNumbers: true,
    bigNumberStrings: true,
    multipleStatements: true,
    dateStrings: true,
  }
}

export const knexConfig = {
  client: 'postgresql',
  debug: env?.environment === 'develop',
  connection: configConnection(),
  pool: {
    min: env.db.dbPoolMin,
    max: env.db.dbPoolMax,
  },
  migrations: {
    tableName: 'migrations',
  },
  seeds: {
    directory: './seeds',
  },
  onCreateTrigger: (): string => `
  CREATE OR REPLACE FUNCTION on_update_timestamp()
  RETURNS TRIGGER AS $$
  BEGIN
      NEW."updated_at" = now();
      RETURN NEW;
  END;
  $$ language 'plpgsql';
  `,
  onUpdateTrigger: (table: string): string => `
    CREATE TRIGGER ${table}_updated_at
    BEFORE UPDATE ON "${table}"
    FOR EACH ROW
    EXECUTE PROCEDURE on_update_timestamp();
  `,
  deleteUpdateTrigger: (table: string): string => `
    DROP TRIGGER IF EXISTS ${table}_updated_at ON "${table}";
  `,
}
