import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class InvalidTokenError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor() {
    const m = 'The token is invalid'
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.INVALID_TOKEN
  }
}
