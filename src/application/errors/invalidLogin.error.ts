import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class InvalidLoginError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor() {
    const m = 'The password or email is invalid'
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.INVALID_LOGIN
  }
}
