import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class UserNotFoundError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor() {
    const m = 'This user not exists or is disable in the system'
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.USER_NOT_FOUND
  }
}
