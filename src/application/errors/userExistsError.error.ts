import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class UserExistsError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor() {
    const m = 'This user already registered'
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.USER_EXISTS_ERROR
  }
}
