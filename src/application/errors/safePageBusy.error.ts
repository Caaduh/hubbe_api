import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class SafePageBusyError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor() {
    const m = 'Safe page busy, try again latter'
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.SAFE_PAGE_BUSY
  }
}
