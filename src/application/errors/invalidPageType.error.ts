import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class InvalidPageTypeError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor() {
    const m = 'The page type is invalid in this app'
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.INVALID_PAGE_TYPE
  }
}
