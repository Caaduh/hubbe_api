import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class NotLoggedOnSafePageError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor() {
    const m = 'You are not logged on safe page'
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.NOT_LOGGED_ON_SAFE_PAGE
  }
}
