import { UserNotFoundError } from '@application/errors'
import { InvalidLoginError } from '@application/errors/invalidLogin.error'
import { left, right } from '@domain/either'
import { User } from '@domain/entity'
import { EitherSignInOutput, SignIn, SignInInput } from '@domain/usecases/auth'
import { CompareProtocol } from '@infrastructure/adapters'
import { JwtEncryptProtocol } from '@infrastructure/adapters/jwt/encrypt.protocol'
import { UserRepositoryProtocol } from '@infrastructure/repositories/protocols/userRepository.protocol'

export class SignInUsecase implements SignIn {
  constructor(
    private readonly userRepository: UserRepositoryProtocol,
    private readonly hashCompare: CompareProtocol,
    private readonly jwt: JwtEncryptProtocol
  ) {}

  async handle(input: SignInInput): Promise<EitherSignInOutput> {
    const signInValidation = User.signIn(input.email, input.password)

    if (signInValidation.isLeft()) {
      return left(signInValidation.value)
    }

    const foundUser = await this.userRepository.getByEmail(
      signInValidation.value.email
    )

    if (foundUser.isLeft()) {
      return left(new UserNotFoundError())
    }

    const passwordIsCorrect = await this.hashCompare.compare(
      signInValidation.value.password,
      foundUser.value.password
    )

    if (!passwordIsCorrect) {
      return left(new InvalidLoginError())
    }

    const { accessToken, refreshToken } = this.jwt.encrypt({
      userId: foundUser.value.id,
    })

    return right({ accessToken, refreshToken })
  }
}
