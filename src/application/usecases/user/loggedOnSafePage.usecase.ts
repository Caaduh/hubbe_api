import { SafePageBusyError } from '@application/errors'
import { InvalidTokenError } from '@application/errors/invalidToken.error'
import { left, right } from '@domain/either'
import { LoggedUser } from '@domain/entity/loggedUser.entity'
import {
  EitherUserLoggedOnSafePageUsecaseOutput,
  UserLoggedOnSafePagePage,
  UserLoggedOnSafePagePageUsecaseInput,
} from '@domain/usecases/user/loggedOnSafePage.protocol'
import { LoggedUserRepositoryProtocol } from '@infrastructure/repositories/protocols'
import { JwtAdapterProtocol } from '@main/adapters'

export class UserLoggedOnSafePageUsecase implements UserLoggedOnSafePagePage {
  constructor(
    private readonly loggedUserRepository: LoggedUserRepositoryProtocol,
    private readonly jwtAdapter: JwtAdapterProtocol
  ) {}

  async handle(
    input: UserLoggedOnSafePagePageUsecaseInput
  ): Promise<EitherUserLoggedOnSafePageUsecaseOutput> {
    const tokenIsValid = this.jwtAdapter.verify(input.authorization)

    if (!tokenIsValid) {
      return left(new InvalidTokenError())
    }

    const userAttributesDecryptedInAuthToken = this.jwtAdapter.decrypt(
      input.authorization
    )

    const iAmLoggedOnSafePage = await this.loggedUserRepository.inSafePage()

    if (
      iAmLoggedOnSafePage.isRight() &&
      iAmLoggedOnSafePage.value.user !==
        userAttributesDecryptedInAuthToken.userId
    ) {
      return left(new SafePageBusyError())
    }

    const iAmNotLoggedOnSafePage = iAmLoggedOnSafePage.isLeft()
    if (iAmNotLoggedOnSafePage) {
      const changeStatusToTrueLoginSafePage = LoggedUser.accessSafePage(
        userAttributesDecryptedInAuthToken.userId
      )
      const connectingUserOrError = await this.loggedUserRepository.insert(
        changeStatusToTrueLoginSafePage
      )

      if (connectingUserOrError.isLeft()) {
        const errorOnConnectedInSafePage = connectingUserOrError.value
        return left(errorOnConnectedInSafePage)
      }
    }

    return right('ConnectedSafePage')
  }
}
