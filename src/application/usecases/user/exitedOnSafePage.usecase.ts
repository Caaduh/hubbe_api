import {
  InvalidTokenError,
  NotLoggedOnSafePageError,
} from '@application/errors'
import { left, right } from '@domain/either'
import { LoggedUser } from '@domain/entity'
import {
  EitherUserExitedOnSafePageUsecaseOutput,
  UserExitedOnSafePage,
  UserExitedOnSafePageUsecaseInput,
} from '@domain/usecases/user/exitedOnSafePage.protocol'
import { LoggedUserRepositoryProtocol } from '@infrastructure/repositories/protocols'
import { JwtAdapterProtocol } from '@main/adapters'

export class UserExitedOnSafePageUsecase implements UserExitedOnSafePage {
  constructor(
    private readonly loggedUserRepository: LoggedUserRepositoryProtocol,
    private readonly jwtAdapter: JwtAdapterProtocol
  ) {}

  async handle(
    input: UserExitedOnSafePageUsecaseInput
  ): Promise<EitherUserExitedOnSafePageUsecaseOutput> {
    const tokenIsValid = this.jwtAdapter.verify(input.authorization)

    if (!tokenIsValid) {
      return left(new InvalidTokenError())
    }

    const userAttributesDecryptedInAuthToken = this.jwtAdapter.decrypt(
      input.authorization
    )

    const loggedUserSafePageExited = LoggedUser.exitSafePage(
      userAttributesDecryptedInAuthToken.userId
    )

    const changeStatusToTrueWhenIamInSafePage = LoggedUser.IamInSafePage(
      userAttributesDecryptedInAuthToken.userId
    )

    const verifyIfUserAreLoggedOnSafePage =
      await this.loggedUserRepository.verifyLoggedSafePage(
        changeStatusToTrueWhenIamInSafePage
      )

    if (verifyIfUserAreLoggedOnSafePage.isLeft()) {
      return left(new NotLoggedOnSafePageError())
    }

    const userExitSafePageOrError =
      await this.loggedUserRepository.exitSafePage(loggedUserSafePageExited)

    if (userExitSafePageOrError.isLeft()) {
      return right('SafePageBusy')
    }

    const userExitedOnSafePage = userExitSafePageOrError.isRight()

    if (userExitedOnSafePage) {
      return right('SafePageReleased')
    }
  }
}
