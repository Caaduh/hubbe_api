import { UserNotFoundError } from '@application/errors'
import { left, right } from '@domain/either'
import {
  EitherGetLoggedUserOutput,
  GetLoggedUser,
  GetLoggedUserUsecaseInput,
} from '@domain/usecases/user'
import { UserRepositoryProtocol } from '@infrastructure/repositories/protocols/userRepository.protocol'

export class GetLoggedUserUsecase implements GetLoggedUser {
  constructor(private readonly userRepository: UserRepositoryProtocol) {}

  async handle(
    input: GetLoggedUserUsecaseInput
  ): Promise<EitherGetLoggedUserOutput> {
    const foundUserOrError = await this.userRepository.get(+input.userId)

    if (foundUserOrError.isLeft()) {
      return left(new UserNotFoundError())
    }

    const foundUser = foundUserOrError.value

    return right({
      fullName: foundUser.fullName,
      email: foundUser.email,
    })
  }
}
