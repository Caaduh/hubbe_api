import { InvalidTokenError } from '@application/errors'
import { left, right } from '@domain/either'
import {
  EitherVerifySafePageStatusUsecaseOutput,
  VerifySafePageStatus,
  VerifySafePageStatusUsecaseInput,
} from '@domain/usecases/user'
import { LoggedUserRepositoryProtocol } from '@infrastructure/repositories/protocols'
import { JwtAdapterProtocol } from '@main/adapters'

export class VerifySafePageStatusUsecase implements VerifySafePageStatus {
  constructor(
    private readonly loggedUserRepository: LoggedUserRepositoryProtocol,
    private readonly jwtAdapter: JwtAdapterProtocol
  ) {}

  async handle(
    input: VerifySafePageStatusUsecaseInput
  ): Promise<EitherVerifySafePageStatusUsecaseOutput> {
    const tokenIsValid = this.jwtAdapter.verify(input.authorization)

    if (!tokenIsValid) {
      return left(new InvalidTokenError())
    }

    const hasSomeoneInSafePage = await this.loggedUserRepository.inSafePage()

    if (hasSomeoneInSafePage.isLeft()) {
      return right('SafePageFree')
    } else {
      return right('SafePageBusy')
    }
  }
}
