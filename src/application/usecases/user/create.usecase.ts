import { UserExistsError } from '@application/errors'
import { left, right } from '@domain/either'
import { User } from '@domain/entity'
import {
  CreateUserAccount,
  CreateUserAccountUsecaseInput,
  EitherCreateUserAccountOutput,
} from '@domain/usecases/user'
import { HashProtocol } from '@infrastructure/adapters'
import { UserRepositoryProtocol } from '@infrastructure/repositories/protocols/userRepository.protocol'

export class CreateUserAccountUsecase implements CreateUserAccount {
  constructor(
    private readonly userRepository: UserRepositoryProtocol,
    private readonly hashAdapter: HashProtocol
  ) {}

  async handle(
    input: CreateUserAccountUsecaseInput
  ): Promise<EitherCreateUserAccountOutput> {
    const userOrError = User.create(input)

    if (userOrError.isLeft()) {
      return left(userOrError.value)
    }

    const user = userOrError.value

    const foundUser = await this.userRepository.getByEmail(user.email)

    if (foundUser.isRight()) {
      return left(new UserExistsError())
    }

    const hashedPassword = await this.hashAdapter.generate(user.password)

    user.password = hashedPassword

    const createUserOnDatabaseOrError = await this.userRepository.save(user)

    if (createUserOnDatabaseOrError.isLeft()) {
      const createdUserError = createUserOnDatabaseOrError.value
      return left(createdUserError)
    }

    return right(true)
  }
}
