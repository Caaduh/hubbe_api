import { Knex } from 'knex'
import { KnexPostgis } from 'knex-postgis'

/* Postgresql Adapter */
export type PostgresqlDatabase = Knex
export type PostgisDatabase = KnexPostgis

export type PostgresqlAdapterConfig = {
  dbConn: PostgresqlDatabase
}

export interface IPostgresqlAdapter {
  db: any | Knex.QueryBuilder
  tableName: string
  dbInstance: Knex
  dbPostgis: PostgisDatabase
}
