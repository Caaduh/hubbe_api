export function mapObjectToSnakeCaseKeys(obj: any): any {
  const result: any = {}
  for (const [key, value] of Object.entries(obj)) {
    const snakeCaseKey = key.replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase()
    result[snakeCaseKey] = value
  }
  return result
}

export function getMappedColumnsFromDatabaseHelper(
  tableName: string,
  columns: string[]
): any {
  let mapped = {}
  const table = tableName
  columns?.forEach(column => {
    const key = `${table}_${column}`
    const value = `${table}.${column}`
    mapped = {
      ...mapped,
      [key]: value,
    }
  })

  return mapped
}

export function filterFields(table: string, data: any) {
  return data.map(item => {
    const filteredItem = {}
    for (const key in item) {
      if (key.startsWith(`${table}_`)) {
        const newKey = key.substring(`${table}_`.length)
        filteredItem[`${table}_${newKey}`] = item[key]
      }
    }

    return filteredItem
  })
}
