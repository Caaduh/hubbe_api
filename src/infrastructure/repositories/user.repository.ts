import { Either, left, right } from '@domain/either'
import { User } from '@domain/entity'
import { Logger } from '@infrastructure/adapters'
import { ProcessEntityError } from '@infrastructure/errors/processEntity.error'
import { mapObjectToSnakeCaseKeys } from '@infrastructure/helpers/mapper'
import { IPostgresqlAdapter } from '@infrastructure/infrastructure.type'
import { UserMapper } from './mappers/user.mapper'
import {
  UserRepositoryPossibleErrorsEither,
  UserRepositoryProtocol,
} from './protocols/userRepository.protocol'

export class UserRepository implements UserRepositoryProtocol {
  constructor(
    private readonly postgresAdapter: IPostgresqlAdapter,
    private readonly logger: Logger
  ) {}

  public async getByEmail(
    email: string
  ): Promise<Either<UserRepositoryPossibleErrorsEither, User>> {
    try {
      const userColumns = UserMapper.getMappedColumnsFromDatabase()

      const data = await this.postgresAdapter.dbInstance
        .table(UserMapper.tableName)
        .columns(userColumns)
        .where({ email })
        .first()

      if (!data) {
        return left(null)
      }
      const user = UserMapper.mapperToEntity(data)

      return right(user)
    } catch (error) {
      this.logger.error(`[X] UserRepository.getByEmail: ${error}`)
      return left(new ProcessEntityError())
    }
  }

  public async get(
    id: number
  ): Promise<Either<UserRepositoryPossibleErrorsEither, User>> {
    try {
      const userColumns = UserMapper.getMappedColumnsFromDatabase()

      const data = await this.postgresAdapter.dbInstance
        .table(UserMapper.tableName)
        .columns(userColumns)
        .where({ id })
        .first()

      if (!data) {
        return left(null)
      }

      return right(UserMapper.mapperToEntity(data))
    } catch (error) {
      this.logger.error(`[X] UserRepository.get: ${error}`)
      return left(new ProcessEntityError())
    }
  }

  async save(entity: User): Promise<Either<ProcessEntityError, number>> {
    try {
      const data = await this.postgresAdapter.dbInstance
        .table(UserMapper.tableName)
        .insert(
          mapObjectToSnakeCaseKeys({
            fullName: entity.fullName,
            email: entity.email,
            password: entity.password,
          })
        )

      if (!data) {
        return left(null)
      }

      return right(data[0])
    } catch (error) {
      this.logger.error(`[X] UserRepository.save: ${error}`)
      return left(new ProcessEntityError())
    }
  }
}
