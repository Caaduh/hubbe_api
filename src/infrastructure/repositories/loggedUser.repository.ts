import { Either, left, right } from '@domain/either'
import { LoggedUser } from '@domain/entity/loggedUser.entity'
import { Logger } from '@infrastructure/adapters'
import { ProcessEntityError } from '@infrastructure/errors/processEntity.error'
import { mapObjectToSnakeCaseKeys } from '@infrastructure/helpers/mapper'
import { IPostgresqlAdapter } from '@infrastructure/infrastructure.type'
import { LoggedUserMapper } from './mappers/loggedUser.mapper'
import { LoggedUserRepositoryProtocol } from './protocols'
import { UserRepositoryPossibleErrorsEither } from './protocols/userRepository.protocol'

export class LoggedUserRepository implements LoggedUserRepositoryProtocol {
  constructor(
    private readonly postgresAdapter: IPostgresqlAdapter,
    private readonly logger: Logger
  ) {}

  public async inSafePage(): Promise<
    Either<UserRepositoryPossibleErrorsEither, LoggedUser>
  > {
    try {
      const columns = LoggedUserMapper.getMappedColumnsFromDatabase()

      const data = await this.postgresAdapter.dbInstance
        .table(LoggedUserMapper.tableName)
        .columns(columns)
        .where({ logged: true })
        .first()

      if (!data) {
        return left(null)
      }

      const loggedUserMapped = LoggedUserMapper.mapperToEntity(data)

      return right(loggedUserMapped)
    } catch (error) {
      this.logger.error(`[X] LoggedUserRepository.inSafePage: ${error}`)
      return left(new ProcessEntityError())
    }
  }

  async insert(
    entity: LoggedUser
  ): Promise<Either<ProcessEntityError, boolean>> {
    try {
      const data = await this.postgresAdapter.dbInstance
        .table(LoggedUserMapper.tableName)
        .insert(
          mapObjectToSnakeCaseKeys({
            userId: entity.user,
            logged: entity.isLogged,
            whichPage: entity.whichPage,
          })
        )

      if (!data) {
        return left(null)
      }

      return right(true)
    } catch (error) {
      this.logger.error(`[X] LoggedUserRepository.insert: ${error}`)
      return left(new ProcessEntityError())
    }
  }

  async exitSafePage(
    entity: LoggedUser
  ): Promise<Either<ProcessEntityError, boolean>> {
    try {
      const data = await this.postgresAdapter.dbInstance
        .table(LoggedUserMapper.tableName)
        .update(
          mapObjectToSnakeCaseKeys({
            userId: entity.user,
            logged: entity.isLogged,
            whichPage: entity.whichPage,
          })
        )
        .where({ user_id: entity.user, which_page: entity.whichPage })

      if (!data) {
        return left(null)
      }

      return right(true)
    } catch (error) {
      this.logger.error(`[X] LoggedUserRepository.exitSafePage: ${error}`)
      return left(new ProcessEntityError())
    }
  }

  async verifyLoggedSafePage(
    entity: LoggedUser
  ): Promise<Either<ProcessEntityError, boolean>> {
    try {
      const data = await this.postgresAdapter.dbInstance
        .table(LoggedUserMapper.tableName)
        .column('id')
        .where({
          user_id: entity.user,
          which_page: entity.whichPage,
          logged: entity.isLogged,
        })
        .first()

      if (!data) {
        return left(null)
      }

      return right(true)
    } catch (error) {
      this.logger.error(
        `[X] LoggedUserRepository.verifyLoggedSafePage: ${error}`
      )
      return left(new ProcessEntityError())
    }
  }

  public async update(
    entity: LoggedUser
  ): Promise<Either<ProcessEntityError, boolean>> {
    try {
      const data = await this.postgresAdapter.dbInstance
        .table(LoggedUserMapper.tableName)
        .where({
          user_id: entity.user,
          which_page: entity.whichPage,
          logged: entity.isLogged,
        })
        .first()

      if (!data) {
        return left(null)
      }

      return right(true)
    } catch (error) {
      this.logger.error(`[X] LoggedUserRepository.update: ${error}`)
      return left(new ProcessEntityError())
    }
  }
}
