import { LoggedUser } from '@domain/entity/loggedUser.entity'
import {
  filterFields,
  getMappedColumnsFromDatabaseHelper,
} from '@infrastructure/helpers/mapper'

export class LoggedUserMapper {
  static tableName = 'logged_users'
  static getColumnsFromDatabase(): string[] {
    const columns = ['id', 'user_id', 'logged', 'created_at', 'updated_at']
    return columns
  }

  static getMappedColumnsFromDatabase() {
    const columns = this.getColumnsFromDatabase()

    return getMappedColumnsFromDatabaseHelper(this.tableName, columns)
  }

  static mapperToEntity(entity): LoggedUser {
    const mapped: LoggedUser = new LoggedUser(
      entity.logged_users_user_id,
      entity.logged_users_is_logged,
      entity.logged_users_which_page
    )
    mapped.id = entity.logged_users_id
    mapped.createdAt = entity.logged_users_created_at
    mapped.modifiedAt = entity.logged_users_updated_at

    return mapped
  }

  static mapperToEntities(entities): LoggedUser[] {
    if (!entities || entities.length === 0) {
      return []
    }

    const filteredEntities = filterFields(this.tableName, entities)
    const result: LoggedUser[] = filteredEntities.map((entity: LoggedUser) =>
      this.mapperToEntity(entity)
    )
    return result
  }
}
