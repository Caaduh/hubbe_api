import { User } from '@domain/entity'
import {
  filterFields,
  getMappedColumnsFromDatabaseHelper,
} from '@infrastructure/helpers/mapper'

export class UserMapper {
  static tableName = 'users'
  static getColumnsFromDatabase(): string[] {
    const columns = [
      'id',
      'full_name',
      'email',
      'password',
      'created_at',
      'updated_at',
    ]
    return columns
  }

  static getMappedColumnsFromDatabase() {
    const columns = this.getColumnsFromDatabase()

    return getMappedColumnsFromDatabaseHelper(this.tableName, columns)
  }

  static mapperToEntity(entity): User {
    const mapped: User = new User(
      entity.users_full_name,
      entity.users_email,
      entity.users_password
    )
    mapped.id = entity.users_id
    mapped.createdAt = entity.users_created_at
    mapped.modifiedAt = entity.users_updated_at

    return mapped
  }

  static mapperToEntities(entities): User[] {
    if (!entities || entities.length === 0) {
      return []
    }

    const filteredEntities = filterFields(this.tableName, entities)
    const result: User[] = filteredEntities.map((entity: User) =>
      this.mapperToEntity(entity)
    )
    return result
  }
}
