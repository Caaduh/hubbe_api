import { Either } from '@domain/either'
import { User } from '@domain/entity'
import { ProcessEntityError } from '@infrastructure/errors/processEntity.error'

export type UserRepositoryPossibleErrorsEither = ProcessEntityError | null

export interface UserRepositoryProtocol {
  getByEmail(
    email: string
  ): Promise<Either<UserRepositoryPossibleErrorsEither, User>>
  get(id: number): Promise<Either<UserRepositoryPossibleErrorsEither, User>>
  save(
    entity: User
  ): Promise<Either<UserRepositoryPossibleErrorsEither, number>>
}
