import { Either } from '@domain/either'
import { LoggedUser } from '@domain/entity'
import { ProcessEntityError } from '@infrastructure/errors/processEntity.error'
import { UserRepositoryPossibleErrorsEither } from './userRepository.protocol'

export type LoggedUserRepositoryPossibleErrorsEither = ProcessEntityError | null

export interface LoggedUserRepositoryProtocol {
  inSafePage(): Promise<Either<UserRepositoryPossibleErrorsEither, LoggedUser>>
  insert(
    entity: LoggedUser
  ): Promise<Either<UserRepositoryPossibleErrorsEither, boolean>>
  exitSafePage(
    entity: LoggedUser
  ): Promise<Either<UserRepositoryPossibleErrorsEither, boolean>>
  verifyLoggedSafePage(
    entity: LoggedUser
  ): Promise<Either<UserRepositoryPossibleErrorsEither, boolean>>
  update(
    entity: LoggedUser
  ): Promise<Either<UserRepositoryPossibleErrorsEither, boolean>>
}
