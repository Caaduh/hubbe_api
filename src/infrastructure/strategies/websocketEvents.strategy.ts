import { UserExitedOnSafePageUsecase } from '@application/usecases/user/exitedOnSafePage.usecase'
import { UserLoggedOnSafePageUsecase } from '@application/usecases/user/loggedOnSafePage.usecase'
import { VerifySafePageStatusUsecase } from '@application/usecases/user/verifySafePageStatus.usecase'
import { Either } from '@domain/either'
import { WebsocketEventsEnum } from '@domain/enums/websocketEvents.enum'
import { makeUserExitedOnSafePageUsecase } from '@main/factories/application/usecases/auth/exitedOnSafePage.factory'
import { makeUserLoggedOnSafePageUsecase } from '@main/factories/application/usecases/auth/loggedOnSafePage.factory'
import { makeVerifyOnSafePageStatusUsecase } from '@main/factories/application/usecases/auth/verifySafePageStatus.factory'

type PossibleResults =
  | ReturnType<UserLoggedOnSafePageUsecase['handle']>
  | ReturnType<UserExitedOnSafePageUsecase['handle']>
  | ReturnType<VerifySafePageStatusUsecase['handle']>

export class WebsocketEventsStrategy {
  private static providerName

  static async handle(
    event: WebsocketEventsEnum,
    input: any
  ): Promise<Either<Error, PossibleResults>> {
    try {
      const usecase = this.chooseUsecaseBasedOnEvent(event)

      if (!usecase) {
        throw new Error('Usecase not found')
      }
      const result = await usecase.handle(input)

      if (result) {
        return result
      }
    } catch (e) {
      console.error(e)
    }
  }

  static chooseUsecaseBasedOnEvent(event: WebsocketEventsEnum) {
    const decisions = {
      [WebsocketEventsEnum.USER_LOGGED_ON_SAFE_PAGE]:
        makeUserLoggedOnSafePageUsecase(),
      [WebsocketEventsEnum.USER_EXITED_ON_SAFE_PAGE]:
        makeUserExitedOnSafePageUsecase(),
      [WebsocketEventsEnum.VERIFY_SAFE_PAGE_STATUS]:
        makeVerifyOnSafePageStatusUsecase(),
    }

    WebsocketEventsStrategy.providerName = decisions[event]

    return WebsocketEventsStrategy.providerName
  }
}
