import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export interface ConvertErrorStatusTypeOnStatusCode {
  execute(errorType: StatusTypeEnum): number
}
