export interface HashProtocol {
  generate(plainText: string, salt?: number): Promise<string>
}
