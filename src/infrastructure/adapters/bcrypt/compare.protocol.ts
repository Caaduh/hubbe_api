export interface CompareProtocol {
  compare(plainText: string, hashedInformation: string): Promise<boolean>
}
