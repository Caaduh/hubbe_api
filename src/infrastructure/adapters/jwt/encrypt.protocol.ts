export type JwtEncryptOutputProtocol = {
  accessToken: string
  refreshToken: string
}

export interface JwtEncryptProtocol {
  encrypt(payload: any): JwtEncryptOutputProtocol
}
