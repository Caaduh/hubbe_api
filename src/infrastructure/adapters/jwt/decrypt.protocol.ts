export type JwtDecryptProtocolOutput = {
  userId: number
  iat: number
  exp: number
}
export interface JwtDecryptProtocol {
  decrypt(token: string): JwtDecryptProtocolOutput
}
