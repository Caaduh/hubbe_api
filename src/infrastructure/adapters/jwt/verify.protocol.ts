import { JWtVerifyAdapterOutput } from '@main/adapters'

export interface JwtVerifyProtocol {
  verify(token: string): boolean | JWtVerifyAdapterOutput
}
