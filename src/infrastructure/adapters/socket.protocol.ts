export interface Socket {
  getConnection(): any
  closeConnection(): void
}
