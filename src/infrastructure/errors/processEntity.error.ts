import { StatusTypeEnum } from '@presenters/enums/errors.enum'

export class ProcessEntityError extends Error {
  message: string
  errorType: StatusTypeEnum
  constructor() {
    const m = 'We could not process this actions in this moment'
    super(m)
    this.message = m
    this.errorType = StatusTypeEnum.PROCESS_ENTITY_ERROR
  }
}
