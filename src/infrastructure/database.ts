import knex, { Knex } from 'knex'
import KnexPostgis from 'knex-postgis'

import { knexConfig } from '../config/dbconfig'

let database: Knex
let knexPostgis: KnexPostgis.KnexPostgis

export default function databaseInstance(): Knex {
  if (!database) {
    database = knex(knexConfig)
  }

  return database
}

export function databasePostgis(): KnexPostgis.KnexPostgis {
  const db = databaseInstance()
  if (!knexPostgis) {
    knexPostgis = KnexPostgis(db)
  }

  return knexPostgis
}
