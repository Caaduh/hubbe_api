import { Knex } from 'knex'
import { knexConfig } from '../src/config/dbconfig'

export async function up(knex: Knex): Promise<void> {
  return knex.schema
    .raw(knexConfig.onCreateTrigger())
    .createTable('users', table => {
      table.increments('id').unique().primary().notNullable()
      table.string('full_name').notNullable()
      table.string('email').unique().notNullable()
      table.string('password').notNullable()
      table.timestamp('created_at').defaultTo(knex.fn.now()).notNullable()
      table.timestamp('updated_at').defaultTo(knex.fn.now()).notNullable()
    })
    .raw(knexConfig.onUpdateTrigger('users'))
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('users')
}
