import { Knex } from 'knex'
import { knexConfig } from '../src/config/dbconfig'

export async function up(knex: Knex): Promise<void> {
  return knex.schema
    .raw(knexConfig.onCreateTrigger())
    .createTable('logged_users', table => {
      table.increments('id').unique().primary().notNullable()
      table.integer('user_id').unsigned().notNullable()
      table.boolean('logged').notNullable()
      table.string('which_page').notNullable()
      table.timestamp('created_at').defaultTo(knex.fn.now()).notNullable()
      table.timestamp('updated_at').defaultTo(knex.fn.now()).notNullable()
      table
        .foreign('user_id')
        .references('users.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
    .raw(knexConfig.onUpdateTrigger('logged_users'))
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('logged_users')
}
